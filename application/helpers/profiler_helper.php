<?php

function test_result($dados)
{
	$dados['token'] = "niYYkdn38ch-cnpj15732896000170-softcoachingnovo";
  	$dados['acao'] = "calculaResultadoFinal";

	$url = "http://www.solides.co/system/webserviceprofiler2015.php";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);

	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);

	$data = json_decode($output, 1);
	$data['curl_info'] = $info;
	return $data;
}

function get_chart_image($options)
{
	$dados['type'] = "image/png";
	$dados['options'] = $options;

	$url = "http://export.highcharts.com";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);

	$output = curl_exec($ch);
	//$info = curl_getinfo($ch);
	curl_close($ch);

	
	return $output;
}

function get_chart_options($tipo_chart, $coordenadas)
{
	$options = "";

	switch ($tipo_chart) {
		case 'perfil_isolado':
			$options = '{
                      "title": {
                        "text": "PERFIL ISOLADO"
                      },
                      "legend": false,
                      "xAxis": {
                        "categories": ["Exe", "Com", "Pla", "Ana"]
                      },
                      "yAxis": {
                        "title": {
                          "text": ""
                        },
                        "plotLines": [{
                          "value": 0,
                          "width": 1,
                          "color": "#808080"
                        }]
                      },
                      exporting: { enabled: false },
                      "tooltip": {
                        "enabled": true
                      },
                      "credits": {
                        "enabled": false
                      },
                      "plotOptions": {
                        "areaspline": {}
                      },
                      "chart": {
                        "defaultSeriesType": "line",
                        width: 200,
        				height: 300
                      },
                      "subtitle": {
                        "text": ""
                      },
                      "colors": ["#0000FF", "#FF4040", "#BEBEBE"],
                      "series": [{
                        "name": "Como a pessoa é",
                        "data": ['.implode(",", $coordenadas[0]).'],
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }, {
                        "name": "Como o ambiente cobra dela ser",
                        "data": ['.implode(",", $coordenadas[1]).'],
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }, {
                        "name": "Ajuste a exigencia do ambiente",
                        "data": ['.implode(",", $coordenadas[2]).'],
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }]
                    }';

			break;
		
		case 'estilo_lideranca':
			$options = '{
                      "title": {
                        "text": "ESTILO DE LIDERANÇA"
                      },
                      "legend": false,
                      "xAxis": {
                        "categories": ["Dom", "Inf", "Con", "For"]
                      },
                      "yAxis": {
                        "title": {
                          "text": ""
                        },
                        "plotLines": [{
                          "value": 0,
                          "width": 1,
                          "color": "#808080"
                        }]
                      },
                      exporting: { enabled: false },
                      "tooltip": {
                        "enabled": true
                      },
                      "credits": {
                        "enabled": false
                      },
                      "plotOptions": {
                        "areaspline": {}
                      },
                      "chart": {
                        "defaultSeriesType": "line",
                        width: 200,
        				height: 300
                      },
                      "subtitle": {
                        "text": ""
                      },
                      "colors": ["#0000FF", "#FF4040", "#BEBEBE"],
                      "series": [{
                        "name": "Como a pessoa é",
                        "data": ['.implode(",", $coordenadas[0]).'],
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }, {
                        "name": "Como o ambiente cobra dela ser",
                        "data": ['.implode(",", $coordenadas[1]).'],
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }]
                    }';
			break;

		case 'lideranca_atual':
			$options = '{
                      "title": {
                        "text": "LIDERANÇA ATUAL"
                      },
                      "legend": false,
                      "xAxis": {
                        "categories": ["Dom", "Inf", "Con", "For"]
                      },
                      "yAxis": {
                        "title": {
                          "text": ""
                        },
                        "plotLines": [{
                          "value": 0,
                          "width": 1,
                          "color": "#808080"
                        }]
                      },
                      exporting: { enabled: false },
                      "tooltip": {
                        "enabled": true
                      },
                      "credits": {
                        "enabled": false
                      },
                      "plotOptions": {
                        "areaspline": {}
                      },
                      "chart": {
                        "defaultSeriesType": "line",
                        width: 200,
        				height: 300
                      },
                      "subtitle": {
                        "text": ""
                      },
                      "colors": ["#0000FF", "#FF4040", "#BEBEBE"],
                      "series": [{
                        "name": "serie1",
                        "data": ['.implode(",", $coordenadas[0]).'],
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }]
                    }';
            break;
        
        case 'competencias':
			$options = '{
                      "title": {
                        "text": "COMPETÊNCIAS"
                      },
                      "legend": false,
                      "xAxis": {
                        "categories": ["Agressividade", "Desenv Relac", "Fac Mudancas", "Extroversao", "Dominancia", "Desenv Trabalho", "Formalidade", "Condescendencia", "Concentracao", "Perfil Tecnico", "Exatidao", "Detalhismo", "Perfil Artistica", "Paciencia", "Empatia", "Sociabilidade", "Entusiasmo", "Cap Sonhar", "Automotivacao", "Multitarefas", "Independencia"]
                      },
                      "yAxis": {
                        "gridLineInterpolation": "polygon",
                        "lineWidth": 1,
                        "max": 100
                      },
                      exporting: { enabled: false },
                      "tooltip": {
                        "enabled": true
                      },
                      "credits": {
                        "enabled": false
                      },
                      "plotOptions": {
                        "areaspline": {}
                      },
                      "chart": {
                        "polar": true,
                        "type": "area",
                        width: 700,
        				height: 500
                      },
                      "subtitle": {
                        "text": ""
                      },
                      "series": [{
                        "lineColor": "#0000FF",
                        "name": "Competências",
                        "data": ['.implode(",", $coordenadas[0]).'],
                        "pointPlacement": "on",
                        "enableMouseTracking": false,
                        "shadow": false,
                        "animation": false
                      }]
                    }';
			break;		
	}
	return $options;
}
