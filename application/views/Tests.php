<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<?php
		$menu = "Toolbox";
		include ('topbar.php');
	?>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">

				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[201]?>
							</span>
							<a href="<?=$this->config->base_url();?>index.php/home/index/add_test"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?=$lang[86]?>
							</button> </span> </a>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:900px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[85]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:30%;"><?=$lang[45]?></th>
											<th style="width:10%;"><?=$lang[39]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
 
										for($r=0;$r<count($list_test);$r++){

										if($list_test[$r]->IDAvaliacao>0){
										?>	<tr>
										<td><?=$list_test[$r]->NomeAvaliacao?></td>
										<td><?=$list_test[$r]->Descfricao?></td>
										<td><?=$list_test[$r]->Objectives?></td>
										
										<td>
											<a class="btn btn-success btn-xs" href="<?=$this->config->base_url();?>index.php/home/index/add_test/<?=$list_test[$r]->IDAvaliacao?>">
											<i class="fa fa-edit"></i> <?=$lang[39]?>
											</a>
										</td>
												
										</tr>
										<?php }}?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>
                </div><!-- end col-md-12 -->
<!---========== Wheels ==========================------------------------------------------->
				
				
<!---========== video ==========================------------------------------------------->
				
				
<!---========== LogBook ==========================------------------------------------------->
							
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

