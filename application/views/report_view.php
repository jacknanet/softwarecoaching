<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<?php
		$menu = $lang[124];
		include ('topbar.php');
	?>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">

<!---==========  Report List ==========================------------------------------------------->			
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[124]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:720px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[34]?></th>
											<th style="width:30%;"><?=$lang[205]?></th>
											<th style="width:30%;"><?=$lang[426]?></th>
											<th style="width:10%;"><?=$lang[427]?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
										for($r=0;$r<count($total_compelete_session);$r++){
										?>
										<tr>
											<td> <?=isset($total_compelete_session[$r]->Date)?$total_compelete_session[$r]->Date:''?> </td>
											<td> <?=isset($total_compelete_session[$r]->Value)?$total_compelete_session[$r]->Value:''?></td>
											<td> <?=isset($total_compelete_session[$r]->a2)?$total_compelete_session[$r]->a2:''?> </td>
											<td> <?=isset($total_compelete_session[$r]->a1)?$total_compelete_session[$r]->a1:''?> </td>
										</tr>
										<?php } ?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

