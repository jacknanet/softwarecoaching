<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<?php
		$menu = "Tests&Questionnaires";
		include ('topbar.php');
	?>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Tests&Questionnaires 
							</span>
                        </div>
                        <div class="panel-body pn" style="min-height:880px;">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[44]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $test_info[0]['NomeAvaliacao'];?> </label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[33]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $test_info[0]['Descfricao'];?> </label>
									</div>
									</div>
									
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[45]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $test_info[0]['Objectives'];?> </label>
									</div>
									</div>
								</div>	
							</div>
							<input type="hidden" name="IDAvaliacao" name="<?=$test_info[$r]['IDAvaliacao']?>">
							<div class="col-md-12"> <hr/> </div> <!-- end col-md-12 hr -->
							<?php $t=0;   $question;for($r=0;$r<count($test_info);$r++){ 
							$t++;    
							if($test_info[$r]['t_a']!="Text"){ 
							$question.="@".$test_info[$r]['IDQuest']."#".$t;   
							}?>
							<input type="hidden" name="IDQuest[]" value="<?=$test_info[$r]['IDQuest']?>"> 
							<input type="hidden" name="iDAlternativa[]" value="<?=$test_info[$r]['iDAlternativa']?>">
							<div class="col-md-12">
							<div class="col-md-10">
								<div class="admin-form">	
									<div class="form-group">
									<div class="col-lg-12">
									<dt> <b> <?=$lang[87]?> &nbsp;(<?=$t?>) &nbsp; &nbsp; <?php echo $test_info[$r]['Question'];?> </b> </dt>
									<dd>
										<?php $options=explode("@", $test_info[$r]['option_list']);
										$answer_list=explode("@", $test_answer[$r]['fillling']);
										if($test_info[$r]['t_a']=="Text"){?> 
										<textarea  class="gui-textarea" name="data<?=$test_info[$r]['IDQuest']?>[]"  required><?=$answer_list[0]?></textarea>
										<?php }else{$total =count($options); for($s=0;$s<$total;$s++){    if($options[$s]!=''){?>	
										<p> 
										<input   <?php if(in_array($options[$s], $answer_list)){echo "checked";} ?> class="data<?=$test_info[$r]['IDQuest']?>" type="<?=$test_info[$r]['t_a']?>" name="data<?=$test_info[$r]['IDQuest']?>[]" value="<?php echo $options[$s];?>" style="opacity:5;" ><span >&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $options[$s];?></span>
										</p>   
										<?php }}}?>
									</dd>
									</div>
									</div>
									
								</div>	
							</div>
							
							<div class="col-md-2">
								<div class="admin-form">
									<div class="form-group">
									<div class="col-lg-12">
									<label> <?=$lang[88]?> (<?php echo $test_info[$r]['Points'];?>) </label>
									</div>
									</div>
								</div>	
							</div>
							</div>
							<div class="col-md-12"><hr/></div>
							<?php }?>
							<div class="col-md-12" align="center">
								<?php if($view==""){    ?>
								<button type="Submit" class="btn active btn-success save_answer" name="answer" id="btnSubmit" onclick="return confirm('Are you sure want to Save');">
									<i class="fa fa-save"></i> Submit
								</button>
								<?php }?>
								<a target="_blank" class="btn btn-success btn-xs" href="<?php echo $this->config->base_url()?>index.php/Testing/pdfreport/<?=$test_info[0]['IDAvaliacao']?>"> <i class="fa fa-eye"></i> <?=$lang[308]?>  </a> 
								<a href="javascript:window.history.back();">
									<button class="btn active btn-warning" type="button">
										<i class="fa fa-warning"></i> Cancel
									</button>
								</a>
							</div>
							
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!---==========  Documents List ==========================------------------------------------------->			
				

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>	
<script src="http://jquery.bassistance.de/validate/additional-methods.js"></script>	
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>	
<script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>	
<script src="http://jquery.bassistance.de/validate/additional-methods.js"></script>
<script>$(document).ready(function() {    $('#btnSubmit').on('click', function(e) {     var data="<?=$question?>";     var tot=data.split("@");     var alert1="";     for(var d=1;d<tot.length;d++){         var ids=tot[d].split("#");	if(ids[0]!=""){        var cnt = $("input[name='data"+ids[0]+"[]']:checked").length;        if (cnt < 1)         {           alert1+='\nSelect at least one Answer from  Question '+ids[1];            e.preventDefault();        }        else {          					}        }                        }    if(alert1.length>2){    alert("please attempt the all questions");}    });});</script>