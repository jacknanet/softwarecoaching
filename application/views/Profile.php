
<!--jqueryvalidation plugin-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/validate/validate.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/additional-methods.js"></script>
<script>
$(document).ready(function(){
	form =  $('#frmfirstprofile');				
	$('#frmfirstprofile').validate({
		submitHandler: function(form){
			var size = $('[type="checkbox"]:checked').length;
			console.log(size);
			if (size == 1) {
				console.log('invalid');
				alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
			}else{
				console.log('valid');
				form.submit();
						
			}
		},
		rules: {
			email:{customemail: true},
			//telefone:{
				//required:true,
				//phoneUS: true
				//}
			},
			messages: {
				email:'Please Enter complete Email ID'
			}
				
		});
	});
</script>
<script>
//	$(function(){
//		form =  $('#frmfirstprofile');
//$(".selector").validate({
//  submitHandler: function(form) {
//    // do other things for a valid form
//    form.submit();
//  }
//});
//		
//		$('#frmfirstprofile').submit(function(e){
//			frmvalid = form.valid();
//			if(!(frmvalid == true) && !(valid == true)) {
//				e.preventDefault();
//			}
//			
//					var size = $('[type="checkbox"]:checked').length;
//					if (size == 1) {
//						valid = false;
//						alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
//					}else{
//						valid = true;
//					}
//					console.log(size);
//					console.log(valid);
//					console.log('frmvalid'+frmvalid);
//					if((frmvalid == true) && (valid == true)) {
//						console.log('submit now');
//							form.submit();
//					}else{
//						form.submit(function(e){
//							e.preventDefault();
//						});
//					}
//					
//		});
//	
//	});
</script>	
	
<?php include ('sidemenu.php');?>
<!-- End: sidemenu -->
   
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<?php
		$menu = "Profiler";
		include ('topbar.php');
	?>

	<script type="text/javascript">
					
	function countchk() {
		var size = $('[type="checkbox"]:checked').length;
		if (size <= 1) {
			return false;
			alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
		}
		console.log(size);
					
	}
	function vcb(){
		box = document.getElementsByTagName('input');
		if (box.length  == 0) {
			alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
			return false;
		}
		for(x = 0; x < box.length; x++) {
			if(box[x].checked) {r
				return true;
			} 
		}
		alert("Por favor, leia as instru&ccedil;&otilde;es e responda ao question&aacute;rio. (Ou feche esta p&aacute;gina se n&atilde;o desejar preencher)");

	}
</script>
	<!-- End: Topbar -->

	<!-- Begin: Content -->
	<!-- Begin: Content -->
	<div id="content" class="animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<h3 style="margin-top:50px;" class="text-center">QUESTION&Aacute;RIO - Parte 1 de 2</h3>
				<hr />
			
				<h4 class="text-center">INSTRU&Ccedil;&Otilde;ES:</h4>
				<ol>
					<li>Marque os itens que voc&ecirc; considera que fazem parte do seu temperamento.</li>
					<li>Se n&atilde;o souber o significado de alguma palavra, pesquise-a em um dicion&aacute;rio.</li>
					<li>N&atilde;o &eacute; necess&aacute;rio pensar muito, marque pela sua intui&ccedil;&atilde;o.</li>
					<li>Lembre-se, n&atilde;o marque o que voc&ecirc; gostaria de ser, mas sim, aquilo que voc&ecirc; realmente &eacute;.</li>
					<li>Analise tanto os seus pontos positivos como os seus pontos negativos e seja o mais sincero poss&iacute;vel.</li>
				</ol>
			</div>
		</div>
		<form id="frmfirstprofile" class="form-horizontal" method="POST" action="<?=$this->config->base_url()?>index.php/profiler/index/2"  name="FrontPage_Form1">
			<div class="row" style="margin-left:0px; margin-right:0px;">
				<div class="col-sm-6">
		
					<div class="form-group">
						<label for="email">Nome: </label>
						<input type="text" maxlength="200" size="42" obrigatorio="1"  value="<?=$get_data_coachee_profiler[0]->Name?>" class="form-control" name="nome" descricao="Nome" required></font>
					</div>
  
					<div class="form-group">
						<label for="email">Email:</label>	
						<input type="email" maxlength="200" size="42" id="email" name="email"  value="<?=$get_data_coachee_profiler[0]->Email1?>"  class="form-control" descricao="E-mail" obrigatorio="1" required></font>	
					</div>

					<div class="form-group">
						<label for="sexo">Sexo:</label>	<br>
						<label >
		                    <input type="radio" name="sexo" value="masculino" /> Masculino
		                </label> 
		                <label >
		                    <input type="radio" name="sexo" value="feminino" /> Feminino
		                </label> 	
					</div>
  
					<div class="form-group">
						<label for="email">Telefone:</label>	
						<input type="text" maxlength="200"  size="27" id="telefone" name="telefone"  value="<?=$get_data_coachee_profiler[0]->Mobile?>" class="form-control" obrigatorio="1" descricao="E-mail">	
					</div>
					</div>
				</div>
		
				<div class="row">
					<div class="col-sm-12">
						<h4 class="text-center">INSTRU&Ccedil;&Otilde;ES:</h4>
						<h3><strong>Leia as instru&ccedil;&otilde;es com aten&ccedil;&atilde;o:</strong></h3>
						<ol>
							<li>Esta pesquisa &eacute; individual, n&atilde;o solicite apoio de outra pessoa para n&atilde;o prejudicar o resultado.</li>
							<li>N&atilde;o interrompa o question&aacute;rio com distra&ccedil;&otilde;es ou conversas. N&atilde;o discuta o question&aacute;rio com pessoas &agrave; sua volta.</li>
							<li>Seja sincero, o objetivo da pesquisa n&atilde;o &eacute; qualificar ou desqualificar.</li>
							<li>Reserve pelo menos 15 minutos para responder ao question&aacute;rio sem interrup&ccedil;&otilde;es.</li>
							<li>Mais instru&ccedil;&otilde;es nas pr&oacute;ximas p&aacute;ginas.</li>
						</ol>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-4">
			            <p align="left"><input type="checkbox" name="contagiante1" value="ON"><font size="2" face="Verdana">Contagiante<br>
			            </font><input type="checkbox" name="audacioso1" value="ON"><font size="2" face="Verdana">Audacioso</font><font face="Verdana" size="1">(ousado)</font><font size="2" face="Verdana"><br>
			            </font><input type="checkbox" name="indeciso1" value="ON"><font size="2" face="Verdana">Indeciso<br>
			            </font><input type="checkbox" name="equilibrado1" value="ON"><font size="2" face="Verdana">Equilibrado<br>
			            </font><input type="checkbox" name="inseguro1" value="ON"><font size="2" face="Verdana">Inseguro<br>
			            </font><input type="checkbox" name="lider1" value="ON"><font size="2" face="Verdana">L&iacute;der<br>
			            </font><input type="checkbox" name="ingenuo1" value="ON"><font size="2" face="Verdana">Ing&ecirc;nuo<br>
			            </font><input type="checkbox" name="leal1" value="ON"><font size="2" face="Verdana">Leal<br>
			            </font><input type="checkbox" name="dedicado1" value="ON"><font size="2" face="Verdana">Dedicado<br>
			            </font><input type="checkbox" name="otimista1" value="ON"><font size="2" face="Verdana">Otimista<br>
			            </font><input type="checkbox" name="eficiente1" value="ON"><font size="2" face="Verdana">Eficiente<br>
			            </font><input type="checkbox" name="habilidoso1" value="ON"><font size="2" face="Verdana">Habilidoso<br>
			            </font><input type="checkbox" name="corajoso1" value="ON"><font size="2" face="Verdana">Corajoso</font><font size="2" face="Verdana"><br>
			            </font><input type="checkbox" name="comunicativo1" value="ON"><font size="2" face="Verdana">Comunicativo<br>
			            </font><input type="checkbox" name="decidido1" value="ON"><font size="2" face="Verdana">Decidido<br>
			            </font><input type="checkbox" name="medroso1" value="ON"><font size="2" face="Verdana">Medroso<br>
			            </font><input type="checkbox" name="ativo1" value="ON"><font size="2" face="Verdana">Ativo<br>
			            </font><input type="checkbox" name="intolerante1" value="ON"><font size="2" face="Verdana">Intolerante<br>
			            </font><input type="checkbox" name="pretensioso1" value="ON"><font size="2" face="Verdana">Pretensioso<br>
			            </font><input type="checkbox" name="persistente1" value="ON"><font size="2" face="Verdana">Persistente<br>
			            </font><input type="checkbox" name="barulhento1" value="ON"><font size="2" face="Verdana">Barulhento<br>
			            </font><input type="checkbox" name="animado1" value="ON"><font size="2" face="Verdana">Animado<br>
			            </font><input type="checkbox" name="pratico1" value="ON"><font size="2" face="Verdana">Pratico<br>
			            </font><input type="checkbox" name="minucioso1" value="ON"><font size="2" face="Verdana">Minucioso<br>
			            </font><input type="checkbox" name="discreto1" value="ON"><font size="2" face="Verdana">Discreto<br>
			            </font><input type="checkbox" name="desconfiado1" value="ON"><font size="2" face="Verdana">Desconfiado<br>
			            </font><input type="checkbox" name="autodisciplinado1" value="ON"><font size="2" face="Verdana">Auto-Disciplinado<br>
			            </font><input type="checkbox" name="entusiasta1" value="ON"><font size="2" face="Verdana">Entusiasta<br>
			            </font><input type="checkbox" name="C821" value="ON"><font size="2" face="Verdana">Resoluto</font><font face="Verdana" size="1">(decidido)</font></p>
					</div>
		
					<div class="col-sm-4">
						<input type="checkbox" name="sensivel1" value="ON"><font size="2" face="Verdana">Sens&iacute;vel<br>
						</font><input type="checkbox" name="critico1" value="ON"><font size="2" face="Verdana">Cr&iacute;tico<br>
						</font><input type="checkbox" name="exigente1" value="ON"><font size="2" face="Verdana">Exigente<br>
						</font><input type="checkbox" name="influenciador1" value="ON"><font size="2" face="Verdana">Influenciador<br>
						</font><input type="checkbox" name="empolgante1" value="ON"><font size="2" face="Verdana">Empolgante<br>
						</font><input type="checkbox" name="desmotivado1" value="ON"><font size="2" face="Verdana">Desmotivado<br>
						</font><input type="checkbox" name="insensivel1" value="ON"><font size="2" face="Verdana">Insens&iacute;vel<br>
						</font><input type="checkbox" name="antisocial1" value="ON"><font size="2" face="Verdana">Anti-social<br>
						</font><input type="checkbox" name="estimulante1" value="ON"><font size="2" face="Verdana">Estimulante<br>
						</font><input type="checkbox" name="inflexivel1" value="ON"><font size="2" face="Verdana">Inflex&iacute;vel<br>
						</font><input type="checkbox" name="vingativo1" value="ON"><font size="2" face="Verdana">Vingativo<br>
						</font><input type="checkbox" name="extrovertido1" value="ON"><font size="2" face="Verdana">Extrovertido<br>
						</font><input type="checkbox" name="orgulhoso1" value="ON"><font size="2" face="Verdana">Orgulhoso<br>
						</font><input type="checkbox" name="teorico1" value="ON"><font size="2" face="Verdana">Te&oacute;rico<br>
						</font><input type="checkbox" name="exagerado1" value="ON"><font size="2" face="Verdana">Exagerado<br>
						</font><input type="checkbox" name="firme1" value="ON"><font size="2" face="Verdana">Firme<br>
						</font><input type="checkbox" name="modesto1" value="ON"><font size="2" face="Verdana">Modesto<br>
						</font><input type="checkbox" name="alegre1" value="ON"><font size="2" face="Verdana">Alegre<br>
						</font><input type="checkbox" name="racional1" value="ON"><font size="2" face="Verdana">Racional<br>
						</font><input type="checkbox" name="rotineiro1" value="ON"><font size="2" face="Verdana">Rotineiro<br>
						</font><input type="checkbox" name="calculista1" value="ON"><font size="2" face="Verdana">Calculista<br>
						</font><input type="checkbox" name="bemquisto1" value="ON"><font size="2" face="Verdana">Bem-Quisto<br>
						</font><input type="checkbox" name="destacado1" value="ON"><font size="2" face="Verdana">Destacado<br>
						</font><input type="checkbox" name="pessimista1" value="ON"><font size="2" face="Verdana">Pessimista<br>
						</font><input type="checkbox" name="metodico1" value="ON"><font size="2" face="Verdana">Met&oacute;dico<br>
						</font><input type="checkbox" name="calmo1" value="ON"><font size="2" face="Verdana">Calmo<br>
						</font><input type="checkbox" name="tranquilo1" value="ON"><font size="2" face="Verdana">Tranquilo<br>
						</font><input type="checkbox" name="espalhafatoso1" value="ON"><font size="2" face="Verdana">Espalhafatoso<br>
						</font><input type="checkbox" name="vaidoso1" value="ON"><font size="2" face="Verdana">Vaidoso
						</font>
					</div>
		
					<div class="col-sm-4">
						<input type="checkbox" name="bemhumorado1" value="ON"><font size="2" face="Verdana">Bem-humorado<br>
						</font><input type="checkbox" name="simpatico1" value="ON"><font size="2" face="Verdana">Simp&aacute;tico<br>
						</font><input type="checkbox" name="impaciente1" value="ON"><font size="2" face="Verdana">Impaciente<br>
						</font><input type="checkbox" name="bomcompanheiro1" value="ON"><font size="2" face="Verdana">Bom Companheiro<br>
						</font><input type="checkbox" name="arrogante1" value="ON"><font size="2" face="Verdana">Arrogante<br>
						</font><input type="checkbox" name="egocentrico1" value="ON"><font size="2" face="Verdana">Egoc&ecirc;ntrico<br>
						</font><input type="checkbox" name="conservador1" value="ON"><font size="2" face="Verdana">Conservador<br>
						</font><input type="checkbox" name="desorganizado1" value="ON"><font size="2" face="Verdana">Desorganizado<br>
						</font><input type="checkbox" name="frio1" value="ON"><font size="2" face="Verdana">Frio<br>
						</font><input type="checkbox" name="cumpridor1" value="ON"><font size="2" face="Verdana">Cumpridor<br>
						</font><input type="checkbox" name="popular1" value="ON"><font size="2" face="Verdana">Popular<br>
						</font><input type="checkbox" name="paciente1" value="ON"><font size="2" face="Verdana">Paciente<br>
						</font><input type="checkbox" name="sentimental1" value="ON"><font size="2" face="Verdana">Sentimental</font><font size="2" face="Verdana"><br>
						</font><input type="checkbox" name="depressivo1" value="ON"><font size="2" face="Verdana">Depressivo<br>
						</font><input type="checkbox" name="reservado1" value="ON"><font size="2" face="Verdana">Reservado<br>
						</font><input type="checkbox" name="energico1" value="ON"><font size="2" face="Verdana">En&eacute;rgico<br>
						</font><input type="checkbox" name="perfeccionista1" value="ON"><font size="2" face="Verdana">Perfeccionista<br>
						</font><input type="checkbox" name="sincero1" value="ON"><font size="2" face="Verdana">Sincero<br>
						</font><input type="checkbox" name="idealista1" value="ON"><font size="2" face="Verdana">Idealista<br>
						</font><input type="checkbox" name="indisciplinado1" value="ON"><font size="2" face="Verdana">Indisciplinado<br>
						</font><input type="checkbox" name="autosuficiente1" value="ON"><font size="2" face="Verdana">Auto-Suficiente<br>
						</font><input type="checkbox" name="exuberante1" value="ON"><font size="2" face="Verdana">Exuberante<br>
						</font><input type="checkbox" name="introvertido1" value="ON"><font size="2" face="Verdana">Introvertido<br>
						</font><input type="checkbox" name="independente1" value="ON"><font size="2" face="Verdana">Independente<br>
						</font><input type="checkbox" name="egoista1" value="ON"><font size="2" face="Verdana">Ego&iacute;sta<br>
						</font><input type="checkbox" name="temeroso1" value="ON"><font size="2" face="Verdana">Temeroso<br>
						</font><input type="checkbox" name="procrastinador1" value="ON"><font size="2" face="Verdana">Procrastinador<br>
						</font><input type="checkbox" name="sarcastico1" value="ON"><font size="2" face="Verdana">Sarc&aacute;stico<br>
						</font><input type="checkbox" name="compreensivo1" value="ON"><font size="2" face="Verdana">Compreensivo</font>
					</div>
				</div>
				<br />
				<div class="row">
					<input type="hidden" name="horainicio" value="<?php echo date('H:i:s'); ?>">
					<p align="center"><input type="submit"  class="btn btn-system" value="    Ir para a Parte Final    " name="B1"></p>
				</div>
			</form>
		</div>
		<!-- End: Content -->  
	</section>
	<!-- End: Content-Wrapper -->






