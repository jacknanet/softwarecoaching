<?php
class query_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        $this->solides_master_db = $this->load->database('solides_master_devel', TRUE); 
    }

    function delete_logbook_question($question_id)
    {
        $query = "update questionslogbook set
ACTIVE='1' where    IDQUESTIONSLOGBOOK='".$question_id."'";
        $this->db->query($query);
    }

    function update_question_order($pre_id, $pre_order_id, $current_id, $current_order_id)
    {
        $query = "update questionslogbook set
ORDERQUESTION='" . $pre_order_id . "' where  IDQUESTIONSLOGBOOK='" . $current_id . "'";
        $this->db->query($query);
        $query = "update questionslogbook set
 ORDERQUESTION='" . $current_order_id . "' where  IDQUESTIONSLOGBOOK='" . $pre_id . "'";
        $this->db->query($query);
    }

    function get_total_logbook_question()
    {
        $query = "select * from questionslogbook where ACTIVE='0' and IDCOACH='" . $_SESSION['coach'] . "' order by ORDERQUESTION asc";
        return $query;
    }

    function get_test_query($id)
    {
        $query = "select 
tests.IDAvaliacao,
tests.NomeAvaliacao,
tests.Descfricao,
tests.Objectives,

test_quest.Question,
test_quest.IDQuest,

test_alternatives.Points,
test_alternatives.option_list,
test_alternatives.t_a,
test_alternatives.iDAlternativa

from  tests 
inner join test_quest on test_quest.IDAvaliacao=tests.IDAvaliacao
inner join test_alternatives on test_alternatives.IDQuest=test_quest.IDQuest

where tests.IDAvaliacao='" . $id . "'";
 return $query;
    }

    function get_payment_query($id)
    {
        $query = "select settings.* from coachee 
inner join  settings on  settings.IDCoach=coachee.IDCoach
where coachee.IDCoachee='" . $id . "'";
        return $query;
    }

    function get_test_answer($id, $coachee)
    {
        $query = "select * from test_answers
    where IDAvaliacao='" . $id . "' and coachee_id='" . $coachee . "'";
        return $query;
    }

    function get_attempte_test($a)
    {
        $query = "select distinct(IDAvaliacao) from test_answers
    where  coachee_id='" . $a . "'";
        return $query;
    }

    function get_result_webserviec($id = "")
    {
        if ($id == "") {
            $query = "SELECT data from profiler where coachee_id=" . $_SESSION['coach'] . " order by data desc";
        }
        else {
            $query = "SELECT data from profiler where coachee_id=" . $id . " order by data desc";
        }

        return $this->db->query($query);
    }

    function get_webserviec_data($nome, $email)
    {
        $this->load->helper("profiler");
        $value = new stdClass();
        $value->idcoachee = $_SESSION['coach'];
        $value->points = "200";
        $value->date = date("Y-m-d");
        $value->hour = date("H:i:s");
        $this->db->insert('log_gamification', $value);
        $token = "nfjkdnunUHlNSDKiniuddknatRRkkUGvjk-api";

        // Here you need inform the language exactly like showed on the exemples below
        // Options: Portugues, Ingles, Espanhol

        $idioma = "Portugues";

        // Here you will inform what do you want to sse on the final result of the questionnary answer
        // Options: Report, Array, Nothing

        $finalresult = "Report";

        // Type of report you want to receive from WebService (if you choose "report" above
        // Options: Synthetic, Regular, Extended

        $reporttype = "Regular";

        // Record info into a Data Base
        // You need to create the table to receive datas. The script to create this table is on installation.txt
        // Strong recomended to choose YES

        $optrecord = "No"; //options: Yes, No to record on data base or not record (only show return)
        $dbaddressip = ""; //ip or address to data base
        $dbname = ""; //name of data base
        $dbuser = ""; //user to log into data base
        $dbpassword = ""; //password to access data base

        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        // VARIABLES SETUP
        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        $dados = $_POST;
        $dados['idioma'] = $idioma;
        $dados['nome'] = $dados['senname'];
        $dados['email'] = $dados['senemail'];
        $dados['idioma'] = $idioma;
        $dados['token'] = $token;
        $dados['horafinal'] = date('H:i:s');

        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        // SENDING DATA TO WEBSERVICE
        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        
        $idclifor = $this->db->query("
            SELECT c.idclifor from coach c
            join coachee e on e.IDCoach = c.IDCoach
            where e.IDCoachee = {$_SESSION['coach']}
        ")->row_array() ['idclifor'];
        
        $creditos = $this->solides_master_db->get_where("PCREDITO", array("IDCLIFOR" => $idclifor, "IDEMP" => 1));

        if($creditos->num_rows() > 0 && (int)$creditos->row_array()['CREDITOS'] > 0){
            $result_test = test_result($dados);
            if ($result_test['curl_info']['http_code'] == 200) {
                
                $filename = self::pdf_result($result_test, 2);
                $this->db->insert("profiler", array(
                    "coachee_id" => $_SESSION['coach'],
                    "data" => $filename,
                    "data_teste" => date("Y-m-d H:i:s") ,
                ));           

                $this->solides_master_db->query("
                    UPDATE PCREDITO set CREDITOS = CREDITOS - 1 
                    WHERE IDCLIFOR = $idclifor 
                    and IDEMP = 1
                ");

                return TRUE;                     
            }            
            else{
                return 2; //falha ao conectar no webservice do profiler
            }
        }            

        return FALSE;
    }

    function self_query($query)
    {
        $data = array();
        $list_data = $this->db->query($query);
        foreach($list_data->result() as $list) {
            $data[] = $list;
        }

        return $data;
    }

    private
    function pdf_result($dados, $tipo_relatorio)
    {
        $timestamp = strtotime("now");
        $dados['img_perfil_isolado'] = "perfil_isolado_$timestamp.png";
        $dados['img_estilo_lideranca'] = "estilo_lideranca_$timestamp.png";
        $dados['img_lideranca_atual'] = "lideranca_atual_$timestamp.png";
        $dados['img_competencias'] = "competencias_$timestamp.png";

        // MONTA IMAGENS DOS GRAFICOS PARA O PDF

        $coordenadas[0] = array(
            $dados['d1pi'],
            $dados['d2pi'],
            $dados['d3pi'],
            $dados['d4pi']
        );
        $coordenadas[1] = array(
            $dados['d5'],
            $dados['d6'],
            $dados['d7'],
            $dados['d8']
        );
        $coordenadas[2] = array(
            $dados['d9'],
            $dados['d10'],
            $dados['d11'],
            $dados['d12']
        );
        file_put_contents("./assets/images/temp/" . $dados['img_perfil_isolado'], get_chart_image(get_chart_options("perfil_isolado", $coordenadas)));
        $coordenadas[0] = array(
            $dados['g1_d1'],
            $dados['g1_d2'],
            $dados['g1_d3'],
            $dados['g1_d4']
        );
        $coordenadas[1] = array(
            $dados['g1_d5'],
            $dados['g1_d6'],
            $dados['g1_d7'],
            $dados['g1_d8']
        );
        file_put_contents("./assets/images/temp/" . $dados['img_estilo_lideranca'], get_chart_image(get_chart_options("estilo_lideranca", $coordenadas)));
        $coordenadas[0] = array(
            $dados['g3_d1'],
            $dados['g3_d2'],
            $dados['g3_d3'],
            $dados['g3_d4']
        );
        file_put_contents("./assets/images/temp/" . $dados['img_lideranca_atual'], get_chart_image(get_chart_options("lideranca_atual", $coordenadas)));
        $coordenadas[0] = array(
            $dados['agressividade'],
            $dados['se_desenvolve_pelo_relacionamento'],
            $dados['facilidade_em_lidar_com_mudancas'],
            $dados['extroversao'],
            $dados['dominancia'],
            $dados['se_desenvolve_pelo_trabalho'],
            $dados['formalidade'],
            $dados['condescendencia'],
            $dados['concentracao'],
            $dados['aptidao_para_habilidades_tecnicas'],
            $dados['exatidao'],
            $dados['detalhismo'],
            $dados['hab_artistica'],
            $dados['paciencia'],
            $dados['empatia'],
            $dados['sociabilidade'],
            $dados['entusiasmo'],
            $dados['capacidade_de_sonhar'],
            $dados['automotivacao'],
            $dados['multi_tarefas'],
            $dados['independencia']
        );
        file_put_contents("./assets/images/temp/" . $dados['img_competencias'], get_chart_image(get_chart_options("competencias", $coordenadas)));
        $this->load->library('mpdf60/mpdf');
        $this->mpdf = new mPDF('', // mode - default ''
        '', // format - A4, for example, default ''
        0, // font size - default 0
        '', // default font family
        15, // margin_left
        15, // margin right
        27, // margin top
        30, // margin bottom
        10, // margin header
        9, // margin footer
        'P'); // L - landscape, P - portrait);
        $this->mpdf->SetDisplayMode('fullpage');
        if ($tipo_relatorio == 2) {

            // Relatorio Extendido APPEND

            $this->mpdf->SetImportUse();
            $path_pdf_append = APPPATH . "third_party/profilers/append_extendido.pdf";
            $this->mpdf->SetImportUse();
            $pagecount = $this->mpdf->SetSourceFile($path_pdf_append);
            for ($i = 1; $i <= ($pagecount); $i++) {
                $this->mpdf->AddPage();
                $import_page = $this->mpdf->ImportPage($i);
                $this->mpdf->UseTemplate($import_page);
            }

            $html = $this->load->view('profiler/relatorio_extendido_view', $dados, TRUE);
        }
        elseif ($tipo_relatorio == 1) {
            $html = $this->load->view('profiler/relatorio_view', $dados, TRUE);
        }

        $this->mpdf->WriteHTML($html);

        // escreve em arquivo

        $file = "pro_" . $_SESSION['coach'] . "_" . $timestamp . ".pdf";
        $filename = APPPATH . "third_party/profilers/$file";
        $this->mpdf->Output($filename, 'F');

        // deleta arquivos temporarios

        unlink($_SERVER["DOCUMENT_ROOT"] . "/" . "assets/images/temp/" . $dados['img_perfil_isolado']);
        unlink($_SERVER["DOCUMENT_ROOT"] . "/" . "assets/images/temp/" . $dados['img_estilo_lideranca']);
        unlink($_SERVER["DOCUMENT_ROOT"] . "/" . "assets/images/temp/" . $dados['img_lideranca_atual']);
        unlink($_SERVER["DOCUMENT_ROOT"] . "/" . "assets/images/temp/" . $dados['img_competencias']);
        return $file;

        // return $this->mpdf->Output("", "S");

    }
}