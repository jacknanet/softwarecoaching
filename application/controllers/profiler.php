<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class profiler extends CI_Controller
{
	public	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->helper('email');
		$this->load->model('coachee_model');
		$this->load->model('AddNewCoach_model');
		$this->load->model('model_data');
		$this->model_data->session_expire_redirect("2");
		$this->load->model('query_model');
		$this->load->model('testing_model');
		$this->load->model('language_model');
		$current_lang = $this->language_model->get_coachee_current_language();
		if ($current_lang[0]->name != "")
		{
			$this->language = $current_lang[0]->name;
		}
		else
		{
			$this->language = "default";
		}
	}

	public	function index($a = "", $b = "")
	{
		$data['lang'] = $this->language_model->get_languages($this->language);
		$this->load->model('sidebar/side_model');
		$data['side'] = $this->side_model->coachee($this->language);
		$image['title'] = 'Profiler';
		if ($a == "")
		{
			$image['im'] = $current_lang = $this->language_model->get_coachee_current_language();
			$data['get_data_coachee_profiler'] = $this->AddNewCoach_model->get_data_coach($id);
			$data['Sessions'] = $this->coachee_model->get_sessions();
			$data['get_data_coachee_profiler'] = $this->AddNewCoach_model->get_data_coach($_SESSION['coach']);

			// print_r($data);
			// exit;

			$this->load->view('header', $image);
			$html_final = $this->query_model->get_result_webserviec();
			if ($html_final->num_rows() > 0)
			{				
				$this->load->view("final1", $data);
			}
			else
			{
				$this->load->view("Profile", $data);
			}

			$this->load->view('footer');
		}
		else
		if ($a == "2")
		{
			$data['name'] = $this->input->post('nome', TRUE);
			$data['email'] = $this->input->post('email', TRUE);
			$data['sexo'] = $this->input->post('sexo', TRUE);
			$image['im'] = $current_lang = $this->language_model->get_coachee_current_language();
			$this->load->view('header', $image);
			$this->load->view("form2_pt.php", $data);
			$this->load->view('footer');
		}
		else
		if ($a == "3")
		{
			$sendname = $this->input->post('senname', TRUE);
			$senemail = $this->input->post('senemail', TRUE);

			$data['info'] = $this->query_model->get_webserviec_data($sendname, $senemail);
			
			if($data['info'] && $data['info'] != 2){
				
				$this->load->view('header', $image);

				$this->load->view("final1", $data);
				$this->load->view('footer');
			}

			else{
				header('Content-Type: text/html; charset=utf-8');	
				$msg = $data['info'] == 2 ? "Falha ao conectar com o webservice do Profiler. Tente novamente." : "Créditos indisponíveis. Peça ao seu Coach para liberar créditos profiler para seu teste.";
				
				echo "<script> alert('$msg'); window.location.href = '".base_url("profiler")."';</script>";
			}
		}
	}

	public function sendreport()
	{

		// print_r($this->input->post());
		// exit;

		$name = $this->input->post('namess');
		$email = $this->input->post('email');
		$this->email->from('dev@ansitdev.com', 'Software Coaching : Profiler Info');
		$this->email->to($email);
		$this->email->subject('test email');

		// $mailbody = $this->load->view('email/stepactivationmail', $data, true);

		$mailbody = $this->query_model->get_webserviec_data($name, $email);

		// print_r($mailbody);

		$this->email->message($mailbody);
		if (!$this->email->send())
		{

			// show_error($this->email->print_debugger());

			print_r($this->email->print_debugger());
		}
		else
		{
			echo "mail sent";
		}
	}

	public function pdfreport()
	{
		$this->load->library('mpdf/mpdf');
		$html_final = $this->query_model->get_result_webserviec();
		$data['info'] = $html_final->result() [0]->data;
		$data['pdf'] = true;
		ob_start();
		$this->load->view('final1', $data);
		$html = ob_get_clean();
		$mpdf = new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function ver_profiler()
	{
		$pdf = $this->query_model->get_result_webserviec()->row_array()['data'];
		$file = "./application/third_party/profilers/$pdf";
		$filename = $pdf;
		
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="' . $filename . '"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($file));
		header('Accept-Ranges: bytes');

		@readfile($file);
	}
}
