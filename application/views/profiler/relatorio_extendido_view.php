<!DOCTYPE html>
<html lang="pt">
<html>
<head>
  <title>Relatório Profiler</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/highcharts-more.js"></script>
  <script src="http://code.highcharts.com/4.0.4/modules/exporting.js"></script>
  <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <style type="text/css">   

    * {
      font-family: 'PT Serif', serif !important;
      -webkit-font-smoothing: antialiased !important;
      -moz-osx-font-smoothing: grayscale !important;
      text-rendering: optimizeLegibility !important;
      text-shadow: 0 0 0 !important;      
    }

    /*body{
      margin-top: 50px;
      margin-bottom: 100px;
      margin-right: 50px;
      margin-left: 50px;
    }*/

    .center {
        margin: auto;
        width: 50%;
        border: 3px solid green;
        padding: 10px;
    }

    .table-borderless > tbody > tr > td,
    .table-borderless > tbody > tr > th,
    .table-borderless > tfoot > tr > td,
    .table-borderless > tfoot > tr > th,
    .table-borderless > thead > tr > td,
    .table-borderless > thead > tr > th {
        border: none;
    }

    .title{
      font-weight: bold;
    }

    .report-header{
      background-image: url("<?php echo base_url() ?>assets/images/report_head.png");
      height: 300px;
      background-size: cover;
    }

    .table-disc{
      padding: 20px;
    }

  </style>
</head>
<body>
  <div class="row">
    <div class="col-xs-12">
      <div class="m-b-md">
        <div class="">
          <img src="<?php echo base_url() ?>assets/images/report_head.png" width="100%">
        </div>
      </div>
    </div>
  </div>
<p>&nbsp;</p>
<div class="row">
  <div class="col-xs-12">
    <table class="table table-borderless">
      <tbody>
        <tr>
          <th>Perfil:</th>
          <td><?php echo $perfil; ?></td>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th>Nome:</th>
          <td>  <?php echo $nome; ?></td>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th>Data:</th>
          <td><?php echo date("d/m/Y - H:i\h", strtotime($data)) ?></td>
          <th>Email:</th>
          <td><?php echo $email; ?></td>
        </tr>
        <tr>
          <th>Energia:</th>
          <td><?php echo number_format($forca_candidato, 2, ",", "."); ?> (<?php echo $nivelenergia;?>)</td>
          <th>IP:</th>
          <td><?php echo number_format($indice_positivo, 2, ", ", "."); ?> (<?php echo $nivelip; ?>)</td>
        </tr>
        <tr>
          <th>IEM:</th>
          <td><?php echo $indicestress; ?> (<?php echo $nivelestress;?>)</td>
          <th>Amplitude:</th>
          <td><?php echo number_format($amplitude, 2, ",", "."); ?> (<?php echo $amplitude_nivel; ?>)</td>
        </tr>
        <tr>
          <th>IA:</th>
          <td><?php echo number_format($ia, 2, ", ", "."); ?> (<?php echo $ia_nivel; ?>)</td>
          <th>TR:</th>
          <td> <?php echo empty($manual) ? "$tempoconsumido ($niveltempo) " : "Preenchimento Manual"; ?></td>
        </tr>
        <tr>
          <th>Moral:</th>
          <td><?php echo number_format($moral, 2, ",", "."); ?> (<?php echo $moral_nivel; ?>)</td>
          <th>IF:</th>
          <td><?php echo $flexibilidade; ?> (<?php echo $flex_nivel; ?>)</td>
        </tr>
        <tr>
          <th>Energia Perfil:</th>
          <td><?php echo $energperfil; ?> (<?php echo $nivel_energperfil; ?>)</td>
          <th>Incitabilidade:</th>
          <td><?php echo $incitabilidade; ?> (<?php echo $nivel_incitab; ?>)</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <table class="table text-center table-disc">
      <thead>
        <tr>
          <th></th>
          <th></th>
          <th class="text-center">Perfil +</th>
          <th class="text-center">Perfil -</th>
          <th class="text-center">Geral</th>
          <th class="text-center">Gráfico do Perfil Geral</th>
          <th></th>
        </tr>
      </thead>
      <tr>
        <td class="text-right">Executor</td>
        <td><b>D</b></td>
        <td><?php echo number_format($perc_executor2colp, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_executor2coln, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_executor, 2, ",", ".") ?> %</div></td>
        <td><progress value="<?php echo $perc_executor ?>" max="100"><?php echo $perc_executor ?>%</meter></td>
        <td class="border-reset"><div class="col-xs-9"><?php echo $nivel_d1pi ?></div></td>
      </tr>
      <tr>
        <td class="text-right">Comunicador</td>
        <td><b>I</b></td>
        <td><?php echo number_format($perc_comunicador2sanp, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_comunicador2sann, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_comunicador, 2, ",", ".") ?> %</div></td>
        <td><progress value="<?php echo $perc_comunicador ?>" max="100"><?php echo $perc_comunicador ?>%</meter></td>
        <td class="border-reset"><div class="col-xs-9"><?php echo $nivel_d2pi ?></div></td>
      </tr>
      <tr>
        <td class="text-right">Planejador</td>
        <td><b>S</b></td>
        <td><?php echo number_format($perc_planejador2flep, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_planejador2flen, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_planejador, 2, ",", ".") ?> %</div></td>
        <td><progress value="<?php echo $perc_planejador ?>" max="100"><?php echo $perc_planejador ?>%</meter></td>
        <td class="border-reset"><div class="col-xs-9"><?php echo $nivel_d3pi ?></div></td>
      </tr>
      <tr>
        <td class="text-right">Analista</td>
        <td><b>C</b></td>
        <td><?php echo number_format($perc_analista2melp, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_analista2meln, 2, ",", ".") ?> %</div></td>
        <td><?php echo number_format($perc_analista, 2, ",", ".") ?> %</div></td>
        <td><progress value="<?php echo $perc_analista ?>" max="100"><?php echo $perc_analista ?>%</meter></td>
        <td class="border-reset"><div class="col-xs-9"><?php echo $nivel_d4pi ?></div></td>
      </tr>
    </table>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <!-- <div class="col-xs-4 col-sm-4"> -->
    <center>
          <img width="210" src="<?php echo base_url() ?>assets/images/temp/<?php echo $img_perfil_isolado?>"/>
        <!-- </div>            -->
     
        

    <!-- <div class="col-xs-4 col-sm-4"> -->
        
          <img width="210"  src="<?php echo base_url() ?>assets/images/temp/<?php echo $img_estilo_lideranca?>"/>
        <!-- </div> -->
                      

    <!-- <div class="col-xs-4 col-sm-4"> -->
        
          <img width="210"  src="<?php echo base_url() ?>assets/images/temp/<?php echo $img_lideranca_atual?>"/>          
        <!-- </div> -->
                      
    </center>

  </div>
</div>

<pagebreak>


<div class="row">
  <div class="col-xs-12">

  <?php for($i = 0; $i < count($textoperfil); $i++):?>
    <div class="prevent-break text-block">
      <p class="title"><?php echo $tituloperfil[$i] ?></p>
      <p><?php echo str_replace(array('Fulana','Fulano'), $primeironome, $textoperfil[$i]); ?></p>
    </div>
  <?php endfor; ?>
      
  </div>
</div>
<pagebreak>
<div class="row page-break">
  <div class="col-xs-12">
    <div class="m-b-md">
      <span class="title">ÁREA DE TALENTO</span>
    </div>
  </div>
  <div class="col-xs-12 text-center">
      
      <img width="600"  src="<?php echo base_url() ?>assets/images/info_talentos.png"/>
    
      <?php echo img(array('src' => "http://www.solides.co/system/grafico.php?stroke=api_".strtotime("now")."&graph=5&dx=$dx&dy=$dy&dx2=$dx2&dy2=$dy2&title=area_talento", 'title' => "area_talento", 'style' => 'padding:5px;width:350px;', 'filename' => "api_".strtotime("now").".jpeg", 'usemap' => '#mapaprofissional', 'align'=>'center', 'id'=>'imagemprofissional')); ?>
  </div>
</div>
<pagebreak>
<div class="row page-break">
  <div class="col-xs-12">
    <div class="m-b-md">
      <span class="title">RODA DE COMPETÊNCIAS</span>
    </div>
  </div>
  <div class="col-xs-12 text-center">
      
    <img width="600"  src="<?php echo base_url() ?>assets/images/info_competencias.png"/>
    <div id="radarChart"></div>
      <img src="<?php echo base_url() ?>assets/images/temp/<?php echo $img_competencias ?>"/> 
  </div>
</div>
<pagebreak>
<div class="row page-break">
  <div class="col-xs-12">
    <div class="m-b-md">
      <span class="title">PREFERÊNCIA CEREBRAL</span>
    </div>
  </div>
  <div class="col-xs-12 text-center">
  

    <?php echo img(array('src' => "http://www.solides.co/system/grafico.php?graph=10&stroke=api_".strtotime("now")."&cerebro_esq=$cerebro_esq&cerebro_dir=$cerebro_dir&cerebro_fre=$cerebro_fre&cerebro_tra=$cerebro_tra&diag_sup_esq=$diag_sup_esq&diag_inf_esq=$diag_inf_esq&diag_inf_dir=$diag_inf_dir&diag_sup_dir=$diag_sup_dir&title=pref_cerebral", 'title' => "pref_cerebral", 'style' => 'padding:5px;width:500px;', 'filename' => "api_".strtotime("now").".jpeg", 'usemap' => '#mapaprofissional', 'align'=>'center', 'id'=>'imagemprofissional')); ?>
  </div>
  <div class="col-xs-12 text-block">
    <p>&nbsp;<br /></p>
    <p>A Teoria de Preferências Cerebrais parte de vários estudos na área de Neurologia sob observações e registros de atividades cerebrais medidas por encefalograma. Nestes estudos, observou-se que regiões específicas do cérebro são acionadas em diferentes tipos de problemas e situações. Vários relatos posteriores (alguns incríveis) de lesões no cérebro e consequências, confirmam a teoria. O Profiler identifica claramente estas regiões e exibe graficamente esta distribuição:</p><ul><li>Hemisfério esquerdo: orientado a detalhe, lógico, sequencial, racional, analítico, objetivo</li><li>Hemisfério direito: generalista, aleatório, intuitivo, subjetivo, aventureiro</li><li>Frontal: reservado, racional, objetivo, formal</li><li>Posterior: social, informal, orientado a relacionamentos</li></ul><p>Você pode fazer a leitura de o quanto a pessoa usa de cada área do cerebro, entendendo que:</p><ul><li>Executor: Frontal Direita</li><li>Comunicador: Posterior Direita</li><li>Planejador: Posterior Esquerda</li><li>Analista: Frontal Esquerda</li></ul>
  </div>
</div>
<pagebreak>
  <div class="row page-break">
    <div class="col-xs-12">
      <div class="m-b-md">
        <span class="title">OBSERVAÇÕES</span>
      </div>
    </div>
    <div class="col-xs-12">
      <table class="obs-write-space">
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>&nbsp;</td></tr>
      </table>
    </div>
  </div>



</body>
</html>