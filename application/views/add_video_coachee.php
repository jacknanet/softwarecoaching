<?php include ('sidemenu.php'); $point = ''; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<?php
		$menu = "Video";
		include ('topbar.php');
	?>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Video 
							</span>
                        </div>
                        <div class="panel-body pn" style="min-height:880px;">
							<div class="col-md-12"> &nbsp; </div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="control-group">
									<iframe width="100%" height="345"
									src="//www.youtube.com/embed/<?php echo $edit_video[0]->Link?>">
									</iframe>
									</div>	
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[302]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $edit_video[0]->Title ?> </label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[33]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $edit_video[0]->Description;?> </label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[303]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $edit_video[0]->Purpose;?> </label>
									</div>
									</div>
								</div>	
							</div>
							<div class="col-md-12"> <hr/> </div>
							<div class="col-md-12" align="center">
								<a href="<?=$this->config->base_url();?>index.php/Testing/">
								<button class="btn active btn-warning" type="button">
									<i class="fa fa-warning"></i> <?=$lang[422]?>
								</button></a>
							</div>
							
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!---==========  Documents List ==========================------------------------------------------->			
				

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<script>
$(".new_add").click(function(){
var total="<?=($credit[0]->Credits-$total_coach[0]->total)?>";
if(total<1 ){
alert("Please Buy Credits First");
return false;

}else{
return true;
}
})
</script>