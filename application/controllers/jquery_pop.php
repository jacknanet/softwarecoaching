<?php

class jquery_pop extends CI_Controller {
var $lang="";

    function __construct() {
        parent::__construct();
        $this->load->model("model_data");
$this->load->model('language_model');
$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
    
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
     }    }

    function goal($id = "") {

        $data['lang']=$this->language_model->get_languages($this->lang);
 $data['goal_data'] = $this->model_data->get_goal_data($id);
        $data['id'] = $id;
        $this->load->view('goal_pop', $data);
    }

    function Sessions($id = "") {
       $data['lang']=$this->language_model->get_languages($this->lang);
  $data['goal_data'] = $this->model_data->get_session_data($id);
        $data['id'] = $id;
        $this->load->view('session_popup', $data);
    }

    function Activities($id = "") {
        $data['lang']=$this->language_model->get_languages($this->lang);
 $data['goal_data'] = $this->model_data->get_Activities_data($id);
        $data['id'] = $id;
        $this->load->view('Activities_popup', $data);
    }
	
	function Test($id = "") {
           $this->load->view('test_pop'); 
		
    }
    
    function option_radio($id = "") {
           $this->load->view('option_radio'); 
		
    }
    
    function option_checkbox($id = "") {
           $this->load->view('option_checkbox'); 
		
    }
    
    

}

?>
