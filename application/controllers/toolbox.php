<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ToolBox extends CI_Controller {
var $lang="";

public function __construct()
{

parent::__construct();
$this->load->model('model_data');
 $this->model_data->session_expire_redirect("1");	
	
$this->load->model('coachee_model');

$this->load->model('model_toolbox');
$this->load->model('AddNewCoach_model');
$this->load->model('test/test_model');
$this->load->model('language_model');
$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}

}


public function index($id="")
{
$image['title'] = 'Toolbox';
$data['lang']=$this->language_model->get_languages($this->lang);
$image['im']=$current_lang=$this->language_model->get_current_language();
$this->load->view('header',$image);

$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coach($this->lang);

$data['list_test']=$this->model_toolbox->get_list_test();
$data['list_wheel']=$this->model_toolbox->get_list_wheel();
//$data['logbook']=$this->test_model->get_list_logbook();
$data['list_video']=$this->model_toolbox->list_video();
$this->load->view("toolbox", $data);
$this->load->view('footer',$image);

}

public function wheel()
{
  $data['lang']=$this->language_model->get_languages($this->lang);
  $image['im']=$current_lang=$this->language_model->get_current_language();
  $this->load->view('header',$image);
  $this->load->model('sidebar/side_model');
  $data['side']=$this->side_model->coach($this->lang);
  $this->load->view("add_wheel",$data);
  $this->load->view('footer',$image);
}
	public function add_wheel($id=''){
  
	if( $id == '')
	{
	  $datawhell = array();
	  $datawhellchild = array();
	  $nameArr = array(); 
	  $descrArr = array();
	  $datawhell['IDCoach'] = $_SESSION['coach'];
	  $datawhell['Name'] = $this->input->post('Name');
	  $datawhell['Description'] = $this->input->post('Description');
	  $inserted_id = $this->model_toolbox->add($datawhell);
	  $nameArr = $this->input->post('Name1');
	  $descrArr = $this->input->post('Description1');
	 
	 if(count($nameArr) > 0)
	 {
		for($i=0;$i<count($nameArr);$i++)
		{
		 $datawhellchild['IDCoach']     = $_SESSION['coach'];
		 $datawhellchild['IDwheel']     = $inserted_id;
		 $datawhellchild['Name']        = $nameArr[$i];
		 $datawhellchild['Description'] = $descrArr[$i];
		 $rss = $this->model_toolbox->add($datawhellchild,1);
		}
		
        $this->session->set_flashdata('msg', 'Successfully inserted!'); 
  		redirect('toolbox');
	 }
	 else
	 {
	    $this->session->set_flashdata('msg', 'Successfully inserted!');
		redirect('toolbox');	 	 
	 }
   } 
   else {
	   
	  $datawhell = array();
	  $datawhellchild = array();
	  $nameArr = array(); 
	  $descrArr = array();
	  $datawhell['IDCoach'] = $_SESSION['coach'];
	  $datawhell['Name'] = $this->input->post('Name');
	  $datawhell['Description'] = $this->input->post('Description');
	  $this->model_toolbox->edit_whell($datawhell,$id);
	  
	  $whell_child_idArr = $this->input->post('whell_child_id');
	  $nameArr = $this->input->post('Name1');
	  $descrArr = $this->input->post('Description1');
	  
	 if(count($whell_child_idArr) > 0)
	 {
		for($i=0;$i<count($whell_child_idArr);$i++)
		{
		 if($whell_child_idArr[$i] == 'insert')
		 {
			 $datawhellchild['IDCoach']     = $_SESSION['coach'];
			 $datawhellchild['IDwheel']     = $id;
			 $datawhellchild['Name']        = $nameArr[$i];
			 $datawhellchild['Description'] = $descrArr[$i];
			 $rss = $this->model_toolbox->add($datawhellchild,1);
		 }
		 else
		 {
			 $datawhellchild['IDCoach']     = $_SESSION['coach'];
			 $datawhellchild['IDwheel']     = $id;
			 $datawhellchild['Name']        = $nameArr[$i];
			 $datawhellchild['Description'] = $descrArr[$i];
			 $rss = $this->model_toolbox->edit_whell_child($datawhellchild,$whell_child_idArr[$i]);

		 }
		}
		 $this->session->set_flashdata('msg', 'Successfully updated!');
		 redirect('toolbox');
	 }
	 else
	 {
	    $this->session->set_flashdata('msg', 'Successfully updated!');
	    redirect('toolbox');
		 
	 }  
 
	   
   }
		
  }
  
 public function edit_wheel($id)
 {
    
	  $data['lang']=$this->language_model->get_languages($this->lang);
	  $image['im']=$current_lang=$this->language_model->get_current_language();
	  $this->load->view('header',$image);
	  $this->load->model('sidebar/side_model');
	  $data['side']=$this->side_model->coach($this->lang);
	  $whellArr = array();
	  $whellArr =$this->model_toolbox->get_wheel($id);
	  $pId = $whellArr[0]->IDwheel;
	  $data['whell_child'] =$this->model_toolbox->get_wheel_child($pId);
	  $data['whell'] = $whellArr;
	  $this->load->view("add_wheel",$data);
	  $this->load->view('footer',$image);
	 
 }

 public function videos_and_movies($id="")
{
  $data['lang']=$this->language_model->get_languages($this->lang);
  $image['im']=$current_lang=$this->language_model->get_current_language();
  $this->load->view('header',$image);
  $this->load->model('sidebar/side_model');
  $data['side']=$this->side_model->coach($this->lang);
  if(isset($_POST['video']))
	{
	if($id==""){
	$this->model_toolbox->video();
	$data['msg']=$this->model_data->submit_msg();
	redirect($this->config->base_url()."index.php/toolbox/?msg=".$data['msg']);

	}else{
	$this->model_toolbox->update_video($id);
	$data['msg']=$this->model_data->update_msg();
	redirect($this->config->base_url()."index.php/toolbox/?msg=".$data['msg']);
	}
	}
	$data['id']=$id;
	if($id!=""){
	$data['edit_video']=$this->model_toolbox->edit_video($id);

	}
	
  $this->load->view("add_video",$data);
  $this->load->view('footer',$image);
}
 


 }





