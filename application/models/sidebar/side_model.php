<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class side_model extends CI_Model {
 function __construct() {
        parent::__construct();
    $this->load->model("language_model");
 }

  
 function coach($lang)
  {
     $rt=$this->language_model->get_languages($lang);

     $data=array(
		array($rt[0],"home/index/Dashboard","glyphicons glyphicons-home"),
		array($rt[1],"home/index/Coachees","glyphicons glyphicons-user_add"),
		array($rt[206],"toolbox/","glyphicons glyphicons-settings"),
		//array($rt[2],"home/index/Tests"),
		//array($rt[125],"log_book"),
		array($rt[3],"Documents","glyphicons glyphicons-book_open"),
		array($rt[425],"home/index/schedule","glyphicons glyphicons-alarm"),
		array($rt[4],"home/index/coachmydata","glyphicons glyphicons-user"),
		array($rt[5],"home/index/Settings","glyphicons glyphicons-cogwheels"),
		array($rt[6],"BuyCredits","glyphicons glyphicons-usd"),
		array($rt[7],"knowledge","glyphicons glyphicons-sun"),
		array($rt[124],"report","glyphicons glyphicons-notes_2"),
		array($rt[8],"home/index/help","glyphicons glyphicons-snowflake"),);
	return $data;
  }


 function coachee($lang)
  {
   $rt=$this->language_model->get_languages($lang);
   
	    $data=array(
		array($rt[0],"home_coachee/","glyphicons glyphicons-home"),
		array($rt[125],"logbook_coachee","glyphicons glyphicons-notes_2"),
		array($rt[201],"Testing/test","glyphicons glyphicons-settings"),
		array($rt[202],"Testing/wheel","glyphicons glyphicons-table"),
		array($rt[300],"Testing/video","glyphicons glyphicons-film"),
		array($rt[9],"profiler","glyphicons glyphicons-user"),
     	//array($rt[109],"wheelif"),
		array($rt[10],"MyActivities","glyphicons glyphicons-sun"),
		//array($rt[11],"Testing"),
		array($rt[3],"Documents1/","glyphicons glyphicons-book_open"),
		array($rt[4],"coacheemydata","glyphicons glyphicons-user"),
		array($rt[14],"Payments","glyphicons glyphicons-usd"));
		return $data;
		
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */