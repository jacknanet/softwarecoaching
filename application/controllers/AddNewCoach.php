<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class AddNewCoach extends CI_Controller {
 var $lang="";
 
 function __construct() {
 parent::__construct();
 $this->load->model('sidebar/side_model');
 $this->load->model('model_data');
 $this->model_data->session_expire_redirect("1");	
 $this->load->model('AddNewCoach_model');
 $this->load->model('home_model');
 $this->load->model('query_model');
 $this->load->model('log_model');
 
 
 
  
 $this->load->model('language_model');
 $current_lang=$this->language_model->get_current_language();
 if($current_lang[0]->name!=""){
 
 $this->lang=$current_lang[0]->name;
 }else{
 $this->lang="default";
 }
 $image['im']=$current_lang=$this->language_model->get_current_language();
 $image['title'] = 'Coachee Records';
 $this->load->view('header',$image);
 
 
 }
 
 public function index($id = "",$type='') {
 
 $data['wheelinfo']=$this->AddNewCoach_model->total_Activities("select * from wheellife where IDCoachee='".$id."'");
 
 $data['lang']=$this->language_model->get_languages($this->lang);
 
 ////////////////////////////////////////////////////////////////////////////
 ////////////////////Get All test By coach
 ///////////////////////////////////////////////////////////////////////////////
  
 $answer_query=$this->query_model->get_attempte_test($id); // attempt test list.
 //echo $answer_query;
 //exit;
 $data['test_answer']=$this->model_data->get_total_list($answer_query);
 $data['coachee_id']=$id;
 $data['test_list']=$this->model_data->get_total_list("select tests.* from  tests inner join coachee on coachee.IDCoach=tests.IDCoach where coachee.IDCoachee='".$id."'");
 ///////////////////////////////////
 
 ///insert for activity
 
 
 $data['list_Goals'] = $this->AddNewCoach_model->list_Goals($id);
 $data['list_Sessions'] = $this->AddNewCoach_model->list_Sessions($id);
 $data['list_Activities'] = $this->AddNewCoach_model->list_Activities($id);
 
 if ($id > 0) {
 $data['get_data_coachee'] = $this->AddNewCoach_model->get_data_coach($id);
 $html_final = $this->query_model->get_result_webserviec($id);
 if($html_final -> num_rows() > 0){
 	$data['profiler'] = $html_final->result()[0]->data;
 }
 }
 
 if(isset($_POST['notepad_data'])){
 $check=$this->model_data->get_total_list("select * from notepad where coachee_id='".$id."'");
 if($check[0]->id<1){
 $this->db->query("insert into notepad set 
 coachee_id='".$id."',notepad='".trim($_POST['notepad_data'])."'");
 }else{
 $this->db->query("update notepad set 
 notepad='".trim($_POST['notepad_data'])."'
 where coachee_id='".$id."'");
 
 }
 }
 $data['side'] = $this->side_model->coach($this->lang);
 
 $data['id']=$id;
 
 $url=$this->config->base_url()."index.php/AddNewCoach/viewfilltoday/";
 $data['result']=$this->log_model->get_question_list_info($id,$url);
 $data['notepad']=$this->model_data->get_total_list("select * from notepad where coachee_id='".$id."'");
 //khetab//
   $this->load->model('model_toolbox');
 
   $coachee_id =  $this->uri->segment(3);
   if($type==3)
   { 
	$list_video_Arr = $this->model_toolbox->get_video_list($coachee_id);
	$data['list_video_Arr']=  $list_video_Arr;
   }
   elseif($type==2)
   {
		$test_list_Arr = $this->model_toolbox->get_test($coachee_id);
		$data['list_test_Arr']=  $test_list_Arr;
   }
   elseif($type==1)
   {
		$whell_list_Arr = $this->model_toolbox->get_whell_list($coachee_id);
		$data['whell_list_Arr']=  $whell_list_Arr;
   }
	else
   {
		$whell_list_Arr = $this->model_toolbox->get_whell_list($coachee_id);
		$data['whell_list_Arr']=  $whell_list_Arr;
   }
   $data['type']= $type ;
   $this->load->view('AddNewCoach', $data);
   $this->load->view('footer');
 }
 
 
 function Activity($a,$id=""){
 
 $data['goalw']=$this->AddNewCoach_model->list_Goals($a);
	  if(isset($_POST['save_discuuss'])){
	  
		$this->home_model->save_discussion();
	 }
 $data['lang']=$this->language_model->get_languages($this->lang);
 
 $data['side']=$this->side_model->coach($this->lang);
 $data['cancel']="index.php/AddNewCoach/index/".$a;
 
 
 if (isset($_POST['activities'])) {
 $data['hide']="yes";
 if ($_POST['IDAtividade'] == "") {
 
 $this->AddNewCoach_model->Activities($a);
 header("Location:".$this->config->base_url().$data['cancel']."?msg=".$this->model_data->submit_msg());
 } else {
 
 $this->AddNewCoach_model->Activities_update($_POST['IDAtividade']);
 ///header("Location:".$this->config->base_url().$data['cancel']."?msg=".$this->model_data->update_msg());
 $data['msg']=$this->model_data->update_msg();
 }
 }
 $data['goal_data'] = $this->model_data->get_Activities_data($id);
  
  $data['email']=$this->model_data->get_total_list("select Email1 from coachee where IDCoachee='".$data['goal_data'][0]->IDCoachee."'");
 
 
 
 $data['discussion']=$this->home_model->get_discussion($id);
 $this->load->view('Activity',$data);
 
 $this->load->view('footer');
 
 }
 
 function Goal($a,$id=""){
 $data['lang']=$this->language_model->get_languages($this->lang);
 
 $data['side']=$this->side_model->coach($this->lang);
 $data['cancel']="index.php/AddNewCoach/index/".$a;
 
 if (isset($_POST['goal'])) {
 
 if ($_POST['IDgoal'] == 0) {
	 
 $this->AddNewCoach_model->Goals($a);
 header("Location:".$this->config->base_url().$data['cancel']."?msg=".$this->model_data->submit_msg());
 }
 else {
 $this->AddNewCoach_model->Goals_update($_POST['IDgoal']);
 header("Location:".$this->config->base_url().$data['cancel']."?msg=".$this->model_data->update_msg());
 }
 }
 $data['goal_data'] = $this->model_data->get_goal_data($id);
 
 $this->load->view('goal',$data);
 
 $this->load->view('footer');
 
 }
 
 
 function session($a,$id=""){
 $data['lang']=$this->language_model->get_languages($this->lang);
 
 $data['side']=$this->side_model->coach($this->lang);
 $data['cancel']="index.php/AddNewCoach/index/".$a;
 
 ///insert for session
 if (isset($_POST['sessions'])) {
 if ($_POST['IDSessoes'] == 0) {
 $this->AddNewCoach_model->Sessions($a);
 header("Location:".$this->config->base_url().$data['cancel']."?msg=".$this->model_data->submit_msg());
 } else {
	 
 $this->AddNewCoach_model->Sessions_update($_POST['IDSessoes']);
 header("Location:".$this->config->base_url().$data['cancel']."?msg=".$this->model_data->update_msg());
 }
 }
 $data['goal_data'] = $this->model_data->get_session_data($id);
   
 $this->load->view('session',$data);
 
 $this->load->view('footer');
 
 }
 
 
 function testing($a,$b,$c){
 $data['lang']=$this->language_model->get_languages($this->lang);
 
 $data['side']=$this->side_model->coach($this->lang);
 
 $query=$this->query_model->get_test_query($a);
 
 $data['view']=$b;
 $data['url']="index.php/AddNewCoach/index/".$c;
 $data['test_info']=$this->model_data->get_total_list($query);
 $answer_query=$this->query_model->get_test_answer($a,$c);
 
 $data['test_answer']=$this->model_data->get_total_list($answer_query);
 
 $this->load->view('question_ans',$data);
 
 $this->load->view('footer');
 
 }
 
 function viewfilltoday($a,$coachee_id){
 $data['lang']=$this->language_model->get_languages($this->lang);
 
 $data['side']=$this->side_model->coach($this->lang);
 $query="SELECT * FROM `questionslogbook` 
 inner join logbook on logbook.IDQUESTIONS=questionslogbook.IDQUESTIONSLOGBOOK
 where logbook.date='".$a."'
 and logbook.IDCOACHEE='".$coachee_id."'
 order by questionslogbook.ORDERQUESTION asc
 ";
 $data['attemp_question']=$this->query_model->self_query($query);
 
 $this->load->view('viewfilltoday',$data);
 
 $this->load->view('footer');
 
 }
	  function changestatus()
	  {
		 $data = array();
		 $this->load->model('model_toolbox');
		 $Type = $this->input->post('Type');
		 $IDtool = $this->input->post('IDtool');
		 $IDCoach = $this->input->post('IDCoach');
		 $IDCoachee = $this->input->post('IDCoachee');
		 $enabledisable_Arr = $this->model_toolbox->enable_disable($IDtool,$IDCoach,$Type,$IDCoachee);
		 //print_r($enabledisable_Arr);
		 //exit;
		 if(count($enabledisable_Arr)< 1)
		 {
			 $data['IDCoach']	= $IDCoach; 
			 $data['IDCoachee']	= $IDCoachee;
			 $data['Type']	    = $Type;  
			 $data['IDtool']  	= $IDtool;
			 $data['Enabled']	= '0';
			 $rss = $this->model_toolbox->enable_disable_entry($data);
			 if($rss)
			 {
				 echo '0';
				 exit;
			 }
			 
		  }
		  else 
		  {
				 
			 if($enabledisable_Arr[0]->Enabled == '1')
			  {
			 
				 $data['IDCoach']	= $IDCoach; 
				 $data['IDCoachee']	= $IDCoachee;  
				 $data['Type']	    = $Type; 
				 $data['Enabled']	= '0';
				 $rss = $this->model_toolbox->update_satus($data,$IDtool,$Type);
				 if($rss)
				 {
				   echo '0';
				   exit;   
				 }
			  }
			 if($enabledisable_Arr[0]->Enabled == '0')
			 {
				 $data['IDCoach']	= $IDCoach; 
				 $data['IDCoachee']	= $IDCoachee;  
				 $data['Type']	    = $Type; 
				 $data['Enabled']	= '1'; 
				 $rss2 = $this->model_toolbox->update_satus($data,$IDtool,$Type);
			   if($rss2)
				{
				   echo '1';
				   exit;   
				}	 
			 }
		  }
		  
	  }
	  
	  /* function get_list_test_whell()
	  {
			 $type = $this->input->post('type');
			 $this->load->model('model_toolbox');
			 if($type == 2)
			 {
			 $coachee_id =  $this->uri->segment(3);
			 $list_test_Arr = $this->model_toolbox->get_test($coachee_id);
			 echo '<pre>'; print_r($list_test_Arr);
			 $str = '';
			 $str .='<table id="sample-table-1" class="table table-striped table-bordered table-hover">';
			 $str .='<thead><tr><th>Name</th><th>Discription</th><th>Objectives</th><th>Action</th></tr></thead>';
			 $str .='<tbody>';
			 if(count($list_test_Arr) > 0) {
			 for($i=0; $i < count($list_test_Arr);$i++) {
			 $str .='<tr>';
			 $str .='<td>' .$list_test_Arr[$i]['NomeAvaliacao'].'</td>';
			 $str .='<td>'.$list_test_Arr[$i]['Descfricao'].'</td>';
			 $str .='<td>'.$list_test_Arr[$i]['Objectives'].'</td>';
			  if ($list_test_Arr[$i]['Enabled'] == '1'  ){
			 $str .='<td id="status_'.$list_test_Arr[$i]['IDAvaliacao'].'" class="status"><a href="javascript:"><img src="'.base_url().'assets/images/tick.png"  
					 alt="Inactive" title="Inactive"/></a></td>';
			 } elseif($list_test_Arr[$i]['Enabled'] == '0') {
			 $str .='<td id="status_'.$list_test_Arr[$i]['IDAvaliacao'].'" class="status"><a href="javascript:"><img src="'.base_url().'assets/images/cross.png" 
					 alt="Inactive" title="Inactive"/></a></td>';
			  } else {
			  $str .='<td id="status_'.$list_test_Arr[$i]['IDAvaliacao'].'" class="status"><a href="javascript:"><img src="'.base_url().'assets/images/tick.png" 
					 alt="Inactive" title="Inactive"/></a></td>';
			 } 
			 $str .='</tr>';	
			 } }
					 
			 $str .='</tbody></table>';
			 echo $str;
			 exit;
		 }
		 else
		 {
			
			 $coachee_id =  $this->uri->segment(3);
			 $whell_list_Arr = $this->model_toolbox->get_whell_list($coachee_id);
			 //echo '<pre>'; print_r($whell_list_Arr);
			 $str = '';
			 $str .='<table id="sample-table-1" class="table table-striped table-bordered table-hover">';
			 $str .='<thead><tr><th>Name</th><th>Discription</th><th>Action</th></tr></thead>';
			 $str .='<tbody>';
			 if(count($whell_list_Arr) > 0) {
			 for($i=0; $i < count($whell_list_Arr);$i++) {
			 $str .='<tr>';
			 $str .='<td>' .$whell_list_Arr[$i]['Name'].'</td>';
			 $str .='<td>'.$whell_list_Arr[$i]['Description'].'</td>';
			 
			 if($whell_list_Arr[$i]['Enabled'] == 1  ){ 
			 $str .='<td id="status_'.$whell_list_Arr[$i]['IDwheel'].'" class="status"><a href="javascript:void(0);">
					<img src="'.base_url().'assets/images/tick.png" alt="Inactive" title="Inactive"/></a></td>';
		   } elseif($whell_list_Arr[$i]['Enabled'] == 0 ) {
			 $str .='<td id="status_'.$whell_list_Arr[$i]['IDwheel'].'" class="status"><a href="javascript:void(0);">
					 <img src="'.base_url().'assets/images/cross.png" alt="Inactive" title="Inactive"/></a></td>';
			  } else {
			  $str .='<td id="status_'.$whell_list_Arr[$i]['IDwheel'].'" class="status"><a href="javascript:void(0);">
					 <img src="'.base_url().'assets/images/tick.png" alt="Inactive" title="Inactive"/></a></td>';
			 } 
			 $str .='</tr>';	
			 } }
					 
			 $str .='</tbody></table>';
			 echo $str;
			 exit;
		 }
	  } */
 
 }
 
 /* End of file welcome.php */
 /* Location: ./application/controllers/welcome.php */