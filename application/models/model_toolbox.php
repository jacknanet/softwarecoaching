<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_toolbox extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   

     function add($data,$id='')
     {
		if($id=='')
		{
		 $this->db->insert('wheel', $data);
		 $inserted_id = $this->db->insert_id();
		 return $inserted_id;
		}
		else
		{
		 $this->db->insert('wheelitens', $data);
		  return true;
		}

     }
	 
	  function edit_whell($data,$id)
      {
		 $this->db->where('IDwheel', $id);
         $this->db->update('wheel', $data);
		 
	  }
	  
	  function update_satus($data,$id,$type)
      {
		 $this->db->where('IDtool',$id);
	     $this->db->where('Type',$type);
		 $this->db->update('enabledtool',$data);
		 return '1';
		 
	  }
	  
	  function edit_whell_child($data,$id)
      {
		 $this->db->where('IDwheelitens', $id);
         $this->db->update('wheelitens', $data);
		 
	  }
	  function enable_disable($IDtool,$IDCoach,$Type,$IDCoachee)
	  {
		$this -> db -> select('*');
		$this -> db -> from('enabledtool');
		$this->db->where('IDtool',$IDtool);
		$this->db->where('Type',$Type);
		$this->db->where('IDCoachee',$IDCoachee);
		$this->db->where('IDCoach',$IDCoach);
		$query = $this -> db -> get();
		return $query->result();  
	  }
	   function enable_disable_entry($data)
	  {
		 $this->db->insert('enabledtool', $data);
		 return true;
	  }
	
	
	function get_list_test()
    {
		$this -> db -> select('*');
		$this -> db -> from('tests');
		$this->db->where('IDCoach',$_SESSION['coach']);
		$query = $this -> db -> get();
		return $query->result();
   
	}
	
		
	//wheel.........................
	 function get_list_wheel()
	{
		$this -> db -> select('*');
		$this -> db -> from('wheel');
		$this->db->where('IDCoach',$_SESSION['coach']);
		$query = $this -> db -> get();
		return $query->result();
	}
	
	function get_whell_list($coachee_id)
	{
    	$data = array();
		$i=0;
		$query = "SELECT * FROM wheel where IDCoach = ".$_SESSION['coach'];
		$result = mysql_query($query);
		if($result)
		{
		  while($row = mysql_fetch_array($result))
		  {
			$data[$i]['IDwheel']= $row['IDwheel']; 
			$data[$i]['IDCoach']= $row['IDCoach'];
			$data[$i]['Name']= $row['Name'];
			$data[$i]['Description']= $row['Description'];
			$query2 = "SELECT Enabled,IDCoachee FROM enabledtool where IDCoach = ".$row['IDCoach']." AND IDCoachee = ".$coachee_id."
				AND IDtool = ".$row['IDwheel']." AND Type = 1 " ;
				$result2 = mysql_query($query2);
				$row2 = mysql_fetch_row($result2);
				if(($row2[0] == 0 || $row2[0] == 1) && ($row2[0] != null))
				{
				if($row2[0] == 0 || $row2[0] == null )
				{
				  $data[$i]['Enabled'] = '0';
				}
				else
				{
				  $data[$i]['Enabled'] = '1';
				}
				}
				else
				{
				  $data[$i]['Enabled'] = 'noaction';
				}
		     $i++;
		  }
		    return $data;
	     }
	 
	}
	function get_video_list($coachee_id)
	{
    	$data = array();
		$i=0;
		$query = "SELECT * FROM videos where IDCoach = ".$_SESSION['coach'];
		$result = mysql_query($query);
		if($result)
		{
		  while($row = mysql_fetch_array($result))
		  {
			$data[$i]['IDvideo']= $row['IDvideo']; 
			$data[$i]['IDCoach']= $row['IDCoach'];
			$data[$i]['Title']= $row['Title'];
			$data[$i]['Description']= $row['Description'];
			$data[$i]['Purpose']= $row['Purpose'];
			$query2 = "SELECT Enabled,IDCoachee FROM enabledtool where IDCoach = ".$row['IDCoach']." AND IDCoachee = ".$coachee_id."
				AND IDtool = ".$row['IDvideo']." AND Type = 3" ;
				$result2 = mysql_query($query2);
				$row2 = mysql_fetch_row($result2);
				if(($row2[0] == 0 || $row2[0] == 1) && ($row2[0] != null))
				{
				if($row2[0] == 0 || $row2[0] == null )
				{
				  $data[$i]['Enabled'] = '0';
				}
				else
				{
				  $data[$i]['Enabled'] = '1';
				}
				}
				else
				{
				  $data[$i]['Enabled'] = 'noaction';
				}
		     $i++;
		  }
		    return $data;
	     }
	 
	}
	
	function get_test($coachee_id)
	{
		
	    $data = array();
		$i=0;
		$ci=& get_instance();
        $ci->load->database();
		$query = "SELECT * FROM tests where IDCoach = ".$_SESSION['coach'];
		$result = mysql_query($query);
		if($result)
		{
		  while($row = mysql_fetch_array($result))
		  {
			$data[$i]['IDAvaliacao']= $row['IDAvaliacao']; 
			$data[$i]['IDCoach']= $row['IDCoach'];
			$data[$i]['NomeAvaliacao']= $row['NomeAvaliacao'];
			$data[$i]['Descfricao']= $row['Descfricao'];
			$data[$i]['Objectives']= $row['Objectives'];
			 $query2 = "SELECT Enabled,IDCoachee FROM enabledtool where IDCoach = ".$row['IDCoach']." AND IDCoachee = ".$coachee_id."
				AND IDtool = ".$row['IDAvaliacao']." AND Type = 2 " ;
				$result2 = mysql_query($query2);
				$row2 = mysql_fetch_row($result2);
				if(($row2[0] == 0 || $row2[0] == 1) && ($row2[0] != null))
				{
				if($row2[0] == 0 || $row2[0] == null )
				{
				  $data[$i]['Enabled'] = '0';
				}
				else
				{
				  $data[$i]['Enabled'] = '1';
				}
				}
				else
				{
				  $data[$i]['Enabled'] = 'noaction';
				}
			  $i++;
		    }
		     return $data;	
	     }
	
	}

	
	function get_wheel($id)
	{
		$this -> db -> select('*');
		$this -> db -> from('wheel');
		$this->db->where('IDCoach',$_SESSION['coach']);
		$this->db->where('IDwheel',$id);
		$query = $this -> db -> get();
		return $query->result();
	}
	function get_wheel_child($id)
	{
		$this -> db -> select('*');
		$this -> db -> from('wheelitens');
		$this->db->where('IDCoach',$_SESSION['coach']);
		$this->db->where('IDwheel',$id);
		$query = $this -> db -> get();
		return $query->result();
	}

	   function update_knowledge1($id)
    {
	    $this->Subject = $_POST['Subject']; 
		$this->Description = $_POST['Description']; 
		$this->Date = $_POST['Date']; 
		$this->db->where("IDknowledge",$id);
		$this->db->update('knowledge', $this);
		
	
    }
	function get_video_coachee_list($IDCoachee,$IDCoach)
    {
		$query = 'select * from videos where IDvideo NOT IN(select IDtool from enabledtool where IDCoachee='.$IDCoachee.' and Type =3 and Enabled=0) and IDCoach='.$IDCoach;
	    $query=$this->db->query($query);
       foreach($query->result() as $row)
	   { 
        $data[]=$row;
	   }
	   return $data;
	
    }
	
    function get_wheel_coachee_list($IDCoachee,$IDCoach)
    {
		$query = 'select * from wheel where IDwheel NOT IN(select IDtool from enabledtool where IDCoachee='.$IDCoachee.' and Type =1 and Enabled=0) and IDCoach=                 '.$IDCoach;
	    $query=$this->db->query($query);
       foreach($query->result() as $row)
	   { 
        $data[]=$row;
	   }
	   return $data;
	
    }
	
	
	
	
	
	function get_list_coachee_list($IDCoachee,$IDCoach)
    {
		$query = 'select * from tests where IDAvaliacao NOT IN(select IDtool from enabledtool where IDCoachee='.$IDCoachee.' and Type =2 and Enabled=0)
		          and IDCoach='.$IDCoach;
	    $query=$this->db->query($query);
       foreach($query->result() as $row)
	   { 
        $data[]=$row;
	   }
	   return $data;
	
    }
	
	
	
	
	
	  function get_question($whell_id)
	  {
	   	$this -> db -> select('*');
		$this -> db -> from('wheel');
		$this->db->where('IDwheel',$whell_id);
		$query = $this -> db -> get();
		return $query->result();
	  }
       function get_question_list($whell_id)
	  {
	   
		$this -> db -> select('*');
		$this -> db -> from('wheelitens');
		$this->db->where('IDwheel',$whell_id);
		$query = $this -> db -> get();
		return $query->result();
	  }
	  function add_whellans($data,$table)
	  {
		 $this->db->insert($table, $data);
		  return true;
	  
	  }
	  
	   function video()
    {
	    $this->IDCoach  = $_SESSION['coach'];
        $this->Link = $this->input->post(addslashes('Link'));
		$this->Title = $this->input->post(addslashes('Title'));
		$this->Description = $this->input->post(addslashes('Description')); 
		$this->Purpose = $this->input->post(addslashes('Purpose')); 
		$this->db->insert('videos', $this);
		
	
    }
	
	 function update_video($id)
    {
	    $this->Link = $this->input->post(stripslashes('Link'));
		$this->Title = $this->input->post(stripslashes('Title'));
		$this->Description = $this->input->post(stripslashes('Description')); 
		$this->Purpose = $this->input->post(stripslashes('Purpose')); 
		$this->db->where("IDvideo",$id);
		$this->db->update('videos', $this);
		
	
    }
	
	function list_video()
	{
	   $role1="";
		$this->db->select('*');
		$this->db->from('videos');
		$this->db->where("IDCoach",$_SESSION['coach']);
		$query = $this->db->get();
		foreach($query->result() as $role)
		{
		$role1[]=$role;
	   
		}
		return $role1;
	  
	}
	
	function edit_video($id)
	{
	   $role1="";
		$this->db->select('*');
		$this->db->from('videos');
		$this->db->where('IDvideo',$id);
		$query = $this->db->get();
		foreach($query->result() as $role)
		{
		$role1[]=$role;
	   
		}
		return $role1;
	  
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */