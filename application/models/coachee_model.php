<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class coachee_model extends CI_Model {


function __construct()
{
	// Call the Model constructor
	parent::__construct();
	$this->solides_master_db = $this->load->database('solides_master_devel', TRUE); 

}



function coachee()
{
if(strlen($_FILES['photo']['name'])>3){
$images=date("Y-m-d-H-i-s").$_FILES['photo']['name'];
move_uploaded_file($_FILES['photo']['tmp_name'],"coach_images/".$images);
}else{
$images='';
}    
$this->Photo=$images;
$this->IDCoach  = $_SESSION['coach'];
$this->Name = $_POST['Name']; 
$this->Matricula = $_POST['Matricula']; 
$this->Email1 = $_POST['Email1']; 
$this->Email2 = $_POST['Email2']; 
$this->Password = $_POST['Password']; 
$this->PhoneResidential = $_POST['PhoneResidential']; 
$this->PhoneCommercial = $_POST['PhoneCommercial']; 
$this->Mobile = $_POST['Mobile']; 
$this->Document = $_POST['Document']; 
$this->CivilState = $_POST['CivilState']; 
$this->Education = $_POST['Education']; 
$this->HierachicalLevel = $_POST['HierachicalLevel']; 
$this->Profession = $_POST['Profession']; 
$this->Company = $_POST['Company'];
$this->Address = $_POST['Address'];
$this->SessoesFeitas = $_POST['SessoesFeitas']; 
$this->Birth =($_POST['Birth']); 
$this->DataFimCoaching = ($_POST['DataFimCoaching']); 
$this->DataInicioCoaching =($_POST['DataInicioCoaching']) ; 
$this->Neighborhood = $_POST['Neighborhood']; 
$this->City = $_POST['City']; 
$this->Country = $_POST['Country']; 
$this->ZipCode = $_POST['ZipCode']; 
$this->Observations = $_POST['Observations']; 
$this->Notes = $_POST['Notes']; 
$this->ObjetivosDoCoaching = $_POST['ObjetivosDoCoaching']; 
$this->Status = $_POST['Status']; 
$this->db->insert('coachee', $this);
$insert_id = $this->test_id=$this->db->insert_id();
$this->coachee_model->sendcoacheeaddmail($insert_id);
//return $insert_id;
//print_r($id);
//exit;

//////////////////////Update Credit in  coach Table;////////////////////////////////////
//$total=$this->get_list_coachee();
//$this->db->query("update coach set Credits='".$total[0]->total."' where IDCoach='".$_SESSION['coach']."'");


}

function update_coachee($id)
{
if(strlen($_FILES['photo']['name'])>3){
$images=date("Y-m-d-H-i-s").$_FILES['photo']['name'];
move_uploaded_file($_FILES['photo']['tmp_name'],"coach_images/".$images);
}else{
$images=$_POST['photo1'];
}    $this->Photo=$images;

$this->IDCoach  = $_SESSION['coach'];
$this->Birth = ($_POST['Birth']); 
$this->DataFimCoaching = ($_POST['DataFimCoaching']); 
$this->DataInicioCoaching =($_POST['DataInicioCoaching']) ; 

$this->Name = $_POST['Name']; 
$this->Matricula = $_POST['Matricula']; 
$this->score = $_POST['score']; 
$this->SessoesFeitas = $_POST['SessoesFeitas']; 
$this->Email1 = $_POST['Email1']; 
$this->Email2 = $_POST['Email2']; 
$this->Password = $_POST['Password']; 
$this->PhoneResidential = $_POST['PhoneResidential']; 
$this->PhoneCommercial = $_POST['PhoneCommercial']; 
$this->Mobile = $_POST['Mobile']; 
$this->Document = $_POST['Document']; 
$this->CivilState = $_POST['CivilState']; 
$this->Education = $_POST['Education']; 
$this->HierachicalLevel = $_POST['HierachicalLevel']; 
$this->Profession = $_POST['Profession']; 
$this->Company = $_POST['Company']; 

$this->Address = $_POST['Address']; 
$this->Neighborhood = $_POST['Neighborhood']; 
$this->City = $_POST['City']; 
$this->Country = $_POST['Country']; 
$this->ZipCode = $_POST['ZipCode']; 
$this->Observations = $_POST['Observations']; 
$this->Notes = $_POST['Notes']; 
$this->ObjetivosDoCoaching = $_POST['ObjetivosDoCoaching']; 
$this->Status = $_POST['Status']; 

$this->db->update('coachee', $this, array('IDCoachee' => $id));

}


function update_coacheemydata($id)
{
if(strlen($_FILES['photo']['name'])>3){
$images=date("Y-m-d-H-i-s").$_FILES['photo']['name'];
move_uploaded_file($_FILES['photo']['tmp_name'],"coach_images/".$images);
}else{
$images=$_POST['photo1'];
}    $this->Photo=$images;

$this->DataFimCoaching = $_POST['DataFimCoaching']; 
$this->DataInicioCoaching = $_POST['DataInicioCoaching']; 

$this->Name = $_POST['Name']; 
$this->score = $_POST['score']; 
$this->SessoesFeitas = $_POST['SessoesFeitas']; 
$this->Email1 = $_POST['Email1']; 
$this->Email2 = $_POST['Email2']; 
$this->Password = $_POST['Password']; 
$this->PhoneResidential = $_POST['PhoneResidential']; 
$this->PhoneCommercial = $_POST['PhoneCommercial']; 
$this->Mobile = $_POST['Mobile']; 
$this->Document = $_POST['Document']; 
$this->CivilState = $_POST['CivilState']; 
$this->Education = $_POST['Education']; 
$this->HierachicalLevel = $_POST['HierachicalLevel']; 
$this->Profession = $_POST['Profession']; 
$this->Company = $_POST['Company']; 
$this->Birth = $_POST['Birth']; 
$this->Address = $_POST['Address']; 
$this->Neighborhood = $_POST['Neighborhood']; 
$this->City = $_POST['City']; 
$this->Country = $_POST['Country']; 
$this->ZipCode = $_POST['ZipCode']; 
$this->Observations = $_POST['Observations']; 
$this->Notes = $_POST['Notes']; 
$this->ObjetivosDoCoaching = $_POST['ObjetivosDoCoaching']; 
$this->Status = $_POST['Status']; 

$this->db->update('coachee', $this, array('IDCoachee' => $id));

}
function list_coachee($id, $matricula = null)
{
$role1="";
$q = "select * from coachee where Status='".$id."' and IDCoach='".$_SESSION['coach']."'";
if ( $matricula ) {
	$q .= "and Matricula='$matricula'";
}
$query = $this->db->query($q);
foreach($query->result() as $role)
{
$role1[]=$role;

}
return $role1;
}



function get_list_languages()
{
$role1="";
$query = $this->db->query("select * from languages");
foreach($query->result() as $role)
{
$role1[]=$role;

}
return $role1;
}

function get_list_coachee()
{
$role1="";
$query = $this->db->query("select count(*) as total from coachee where  IDCoach='".$_SESSION['coach']."'");
foreach($query->result() as $role)
{
$role1[]=$role;

}
return $role1;
}
function get_test()
{
$role1="";
$query = $this->db->query("select count(*) as total from tests where  IDCoach='".$_SESSION['coach']."'");
foreach($query->result() as $role)
{
$role1[]=$role;

}
return $role1;
}

	function get_coach_credit()
	{
		$query = $this->db->query(" SELECT idclifor FROM coach where IDCoach = {$_SESSION['coach']} ");

		if($query->num_rows() > 0){
			$idclifor = $query->row_array()['idclifor'];
			
			if($idclifor > 0){
				return $this->solides_master_db->query("
					SELECT CREDITOS FROM PCREDITO
					WHERE IDCLIFOR = $idclifor
					AND IDEMP = 1
				")->row_array()['CREDITOS'];
			}
		}

		return "-";
	}



function get_coach_documents()
{
$role1="";
$query = $this->db->query("select count(*) as total from documents where   IDCoach='".$_SESSION['coach']."'");
foreach($query->result() as $role)
{
$role1[]=$role;

}
return $role1;
}


function add_settings()
{
if(strlen($_FILES['Logo']['name'])>3){
$images=date("Y-m-d-H-i-s").$_FILES['Logo']['name'];
move_uploaded_file($_FILES['Logo']['tmp_name'],"coach_images/".$images);
}else{
$images=$_POST['photo1'];
}    
$this->Logo=$images;
$this->IDCoach  = $_SESSION['coach'];
$this->Language = $_POST['Language']; 
$this->code = $_POST['code']; 
$this->ContaPaypal = $_POST['ContaPaypal']; 
$this->db->insert('settings', $this);




}

function update_settings($id)
{
if(strlen($_FILES['Logo']['name'])>3){
$images=date("Y-m-d-H-i-s").$_FILES['Logo']['name'];
move_uploaded_file($_FILES['Logo']['tmp_name'],"coach_images/".$images);
}else{
$images=$_POST['photo1'];
}    
if($_POST['code']=='')
    $is_google_calander='0';
else
    $is_google_calander='1';

$this->db->where("IDConfig",$id);

$data = array(
               'Logo' => $images,
               'IDCoach' => $_SESSION['coach'],
               'code' => $_POST['code'],
               'Language'=>$_POST['Language'],
               'ContaPaypal'=>$_POST['ContaPaypal'],
                'is_google_calander'=>$is_google_calander
            );

$this->db->update('settings', $data);

}
function get_setting()
{
$role1="";

$query = $this->db->query("select * from settings where   IDCoach='".$_SESSION['coach']."'");
foreach($query->result() as $role)
{
$role1[]=$role;

}

return $role1;}


function get_sessions()
{
$role1="";

$query = $this->db->query("select count(*) as total from sessions where   IDCoach='".$_SESSION['coach']."'");
foreach($query->result() as $role)
{
$role1[]=$role;

}

return $role1;}

function get_tips()
{
	$query = 'select * from tips order by rand() limit 1';
	$query=$this->db->query($query);
    foreach($query->result() as $row)
	{ 
     $data[]=$row;
	}
	return $data;
}

function color_them()
	{
	   $role1="";
		$this->db->select('*');
		$this->db->from('enable_tips');
		$this->db->where('CoachID',$_SESSION['coach']);
		$query = $this->db->get();
		foreach($query->result() as $role)
		{
		$role1[]=$role;
	   
		}
		return $role1;
	  
	}
	
	function select_tip()
	{
	   $role1="";
		$this->db->select('*');
		$this->db->from('enable_tips');
		$this->db->where('CoachID',$_SESSION['coach']);
		$query = $this->db->get();
		foreach($query->result() as $role)
		{
		$role1[]=$role;
	   
		}
		return $role1;
	  
	}
     // For Sending mail For Registration(16/3/2016)
	  public function sendcoacheeaddmail($insert_id){
			
			$userarr = array();
			$userarr = $this->getcoacheedetail($insert_id);
			
			$data['Email'] = $userarr->Email1;
			$data['Password'] = $userarr->Password;
			
			
			$this->email->from('dev@ansitdev.com',' New Coaching System');
			$this->email->to($data['Email']);
			 //print_r($data);
			//exit;
			
		 
			$this->email->subject('Registration Confirmation: New Coaching System');
			
			//$mailbody = $this->load->view('email/newuseraddnotification', $data, true);
			$mailbody = $this->load->view('email/newcoacheaddnotification', $data, true);
			$this->email->message($mailbody);
			if($this->email->send()){
				return true;
			}
			}
			
			
			
            function getcoacheedetail($insert_id){
			   $this->db->select('Email1,Password,Mobile');
			   $query = $this->db->get_where('coachee',array('IDCoachee'=>$insert_id));
			 //echo $this->db->last_query();
			//exit;
			   if($query->num_rows()>0){
				 return $query->row(); 
			   }
			   
			}
	
	
	


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */