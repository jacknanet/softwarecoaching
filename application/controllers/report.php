<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report extends CI_Controller {
var $lang="";

public function __construct()
{

parent::__construct();
$this->load->model('model_data');
$this->model_data->session_expire_redirect("1");	
$this->load->model('language_model');
$this->load->model('AddNewCoach_model');

$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}

}
public function index($a="",$b="")
{
$image['title'] = 'Reports';
$data['lang']=$this->language_model->get_languages($this->lang);
$image['im']=$current_lang=$this->language_model->get_current_language();
$data['total_compelete_session']=$this->AddNewCoach_model->total_session("SELECT coach.Name AS a1,coachee.Name AS a2, payments.*
FROM payments
INNER JOIN coachee ON coachee.IDCoachee=payments.IDCoachee
INNER JOIN coach ON coach.IDCoach=payments.IDCoach
WHERE payments.IDCoach='".$_SESSION['coach']."' AND payments.status='Success'    order by payments.Date,payments.Hour desc");

$this->load->view('header',$image);
$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coach($this->lang);
if(isset($_POST['search'])){
$a=$this->model_data->save_tmp_credit();
$data['view']=$a;
$data['amount']=$_POST['amount_2'];
}

$this->load->view("report_view",$data);
$this->load->view('footer',$image);

}



}
