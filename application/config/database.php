<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(ENVIRONMENT == 'production') {
	$active_group = 'producao';
} else {
	$active_group = 'default';
}

$active_record = TRUE;

$db['producao']['hostname'] = '169.57.152.222';
$db['producao']['username'] = 'swcoachi_coachin';
$db['producao']['password'] = 'S0p@raA13e@rThu!';
$db['producao']['database'] = 'swcoachi_pcoaching';
$db['producao']['dbdriver'] = 'mysql';
$db['producao']['dbprefix'] = '';
$db['producao']['pconnect'] = TRUE;
$db['producao']['db_debug'] = TRUE;
$db['producao']['cache_on'] = FALSE;
$db['producao']['cachedir'] = '';
$db['producao']['char_set'] = 'utf8';
$db['producao']['dbcollat'] = 'utf8_general_ci';
$db['producao']['swap_pre'] = '';
$db['producao']['autoinit'] = TRUE;
$db['producao']['stricton'] = FALSE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'swcoachi_pcoaching',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
/* End of file database.php */
/* Location: ./application/config/database.php */

$db['solides_master_devel'] = array(
	'dsn'	=> '',
	'hostname' => 'solides.adm.br',
	'username' => 'solidesa_rh',
	'password' => 'S@vaSolides15',
	'database' => 'solidesa_rh__master',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => FALSE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);