<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="javascript:void(0);">Dashboard</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
       
			<form class="form-horizontal" role="form">
			 <div class="row">

                <a href="<?=$this->config->base_url()?>index.php/MyActivities" style="color:black"><div class="col-sm-6 col-xl-3">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"><?=$lang[32]?></h1>
							<h1 class="text-system"><?=$total_activitiestcomplete[0]->total?>/<?php 
							if($total_activitiest[0]->total==""){
						echo 0;
						}else{
						echo $total_activitiest[0]->total;
						}
						?></h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons-user_add"></i>
							<b><?=$lang[32]?> </b>
							</span>
						</div>
					</div>
				</div></a>
				
				
				<a href="#"><div class="col-sm-6 col-xl-3" style="color:black">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"> <?=$lang[30]?> </h1>
							<h1 class="text-system"> <?=$complete_goals?>/<?php 
						if($total_goals[0]->total==""){	echo 0;}else{echo $total_goals[0]->total;}
						
						?> </h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons-usd"></i>
							<b><?=$lang[30]?></b>
							</span>
						</div>
					</div>
				</div></a>
				
				</div>
				 <div class="row">
				
				<a href="#"><div class="col-sm-6 col-xl-3" style="color:black">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"> <?=$lang[69]?> </h1>
							<h1 class="text-system"> <?=$total_score[0]->total?> </h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons glyphicons-book_open"></i>
							<b><?=$lang[69]?></b>
							</span>
						</div>
					</div>
				</div></a>

				
				<a href="#" style="color:black"><div class="col-sm-6 col-xl-3"style="color:black">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"> <?=$lang[31]?> </h1>
							<h1 class="text-system"> <?php 
					   if($total_compelete_session[0]->total==""){echo 0;}else{echo $total_compelete_session[0]->total;}
						echo "/";
						if($total_session[0]->total==""){echo 0;}else{ echo $total_session[0]->total;}
						
						
						?> </h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons-notes_2"></i>
							<b><?=$lang[31]?></b>
							</span>
						</div>
					</div>
				</div></a>
			
				</div>
				 <div class="row">
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span>Next <?=$lang[32]?> 
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:300px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:15%;"><?=$lang[36]?></th>
											<th style="width:25%;">Deadline </th>
											<th style="width:20%;"><?=$lang[24]?></th>
											<th style="width:5%;"><?=$lang[102]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
											for($r=0;$r<count($list_Activities);$r++){
											if($list_Activities[$r]->IDAtividade>0){
											?>	
											<tr>
											<td><?php echo ($list_Activities[$r]->StartDate)?></td>
											<td><?php echo($list_Activities[$r]->Deadline)?></td>
											<td><?php echo $list_Activities[$r]->Activity?></td>
											<td><a class="btn btn-success btn-xs" href="<?=$this->config->base_url()?>index.php/home_coachee/Activity/<?php echo $list_Activities[$r]->IDAtividade?>/view"> <i class="fa fa-eye"></i> <?=$lang[102]?>  </a></td>
											</tr>
										<?php }} ?>
										
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				</a>
				 <div class="row">
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[30]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:300px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:15%;"><?=$lang[33]?></th>
											<th style="width:25%;"><?=$lang[50]?> </th>
											<th style="width:20%;"><?=$lang[51]?></th>
											<th style="width:5%;"><?=$lang[102]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
										for($r=0;$r<count($list_goal);$r++){
										if($list_goal[$r]->IDgoal>0){
										?>	
												<tr>
												<td><?php echo ($list_goal[$r]->Description)?></td>
												<td><?php echo ($list_goal[$r]->Evidence)?></td>
												<td><?php echo $list_goal[$r]->motivators?></td>
												<td><a class="btn btn-success btn-xs" href="<?=$this->config->base_url()?>index.php/home_coachee/Goal/<?php echo $list_goal[$r]->IDgoal?>"> <i class="fa fa-eye"></i> <?=$lang[102]?>  </a></td>
												</tr>
												
										<?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
					</div>
				 <div class="row">
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[31]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:300px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><?=$lang[34]?></th>
											<th><?=$lang[27]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
				
										for($r=0;$r<count($list_session);$r++){
										if($list_session[$r]->IDSessoes>0){
										?>	
											<tr>
												<td><?php echo date("d/m/Y",strtotime($list_session[$r]->Date))?></td>
												<td><?php echo $list_session[$r]->complete?></td>
												<!---<td><a href="<?=$this->config->base_url()?>index.php/home_coachee/session/<?php echo $list_session[$r]->IDSessoes?>">View</a></td>----->
											</tr>
										<?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				 </div>	
			</form>      
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->




