<!DOCTYPE html>
<html>
<head>
     <link href="http://localhost:1234/vik/kendo.common.min.css" rel="stylesheet" />
    <link href="http://localhost:1234/vik/kendo.default.min.css" rel="stylesheet" />
    <link href="http://localhost:1234/vik/kendo.dataviz.min.css" rel="stylesheet" />
    <link href="http://localhost:1234/vik/kendo.dataviz.default.min.css" rel="stylesheet" />
    <script src="http://localhost:1234/vik/jquery.min.js"></script>
    <script src="http://localhost:1234/vik/kendo.all.min.js"></script>
</head>
<body>
    <div id="example">
    <div class="demo-section k-content">
        <div id="chart"></div>
    </div>
    <script>
        function createChart() {
            $("#chart").kendoChart({
                title: {
                    text: "Market Value of Major Banks"
                },
                legend: {
                    position: "top"
                },
                seriesDefaults: {
                    type: "radarLine"
                },
                series: [{
                    name: "Market value as of 2007",
                    data: [1, 2, 3, 4, 5, 6,7,8,9,10]
                }],
                categoryAxis: {
                    categories: ["Santander", "JP Morgan", "HSBC", "Credit Suisse",
                        "Goldman Sachs", "Morgan Stanley","Santander", "JP Morgan", "HSBC", "Credit Suisse",
                        "Goldman Sachs"]
                },
                valueAxis: {
                    labels: {
                        format: "{0}"
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0} bln"
                }
            });
        }

        $(document).ready(createChart);
        $(document).bind("kendo:skinChange", createChart);
    </script>
</div>


</body>
</html>
