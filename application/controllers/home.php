<?php if (!defined('BASEPATH'))    exit('No direct script access allowed');

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//ini_set('html_errors', 1);
class Home extends CI_Controller {

    var $lang = "";

    public function __construct() {
        parent::__construct();
        $this->load->model('model_data');
        $this->model_data->session_expire_redirect("1");

        $this->load->model('coachee_model');
        $this->load->model('AddNewCoach_model');
        $this->load->model('test/test_model');
        $this->load->model('language_model');
        $current_lang = $this->language_model->get_current_language();
        if ($current_lang[0]->name != "") {
            $this->lang = $current_lang[0]->name;
        } else {
            $this->lang = "default";
        }

// Your own constructor code
    }

    public function index($a = "", $b = "") {
        $image['title'] = $a;
        $data['lang'] = $this->language_model->get_languages($this->lang);
        $image['im'] = $current_lang = $this->language_model->get_current_language();
        $this->load->view('header', $image);
        $this->load->model('sidebar/side_model');

        $data['side'] = $this->side_model->coach($this->lang);

        if (isset($_POST['submit_coach'])) {
            $this->model_data->coach_update($_SESSION['coach']);
            $data['update'] = $this->model_data->update_msg();
        }
        if ($a == "Coachees") {

            $data['active_coachee'] = $this->coachee_model->list_coachee(1, $_GET['Matricula']);
            $data['inactive_coachee'] = $this->coachee_model->list_coachee(0, $_GET['Matricula']);
            $data['total_coach'] = $this->AddNewCoach_model->total_Activities("select count(*) total from coachee where IDCoach='" . $_SESSION['coach'] . "'");
            $data['credit'] = $this->AddNewCoach_model->total_Activities("select Credits from coach where  	IDCoach='" . $_SESSION['coach'] . "'");


            //print_r( $data['credit']);
//print_r($data['total_coach']);
            $data['active_total'] = $this->model_data->get_total_activiy_count($data['active_coachee']);
            $data['active_total_done'] = $this->model_data->get_total_done_activiy_count($data['active_coachee']);
            $data['inactive_total'] = $this->model_data->get_total_activiy_count($data['inactive_coachee']);
            $data['inactive_total_done'] = $this->model_data->get_total_done_activiy_count($data['inactive_coachee']);

            $data['active_session_total'] = $this->model_data->get_total_session_count($data['active_coachee']);
            $data['active_session_done_total'] = $this->model_data->get_total_done_session_count($data['active_coachee']);
            $data['inactive_session_total'] = $this->model_data->get_total_session_count($data['inactive_coachee']);
            $data['inactive_session_done_total'] = $this->model_data->get_total_done_session_count($data['inactive_coachee']);
        }
        if ($a == "coachmydata") {

            $data['total_coach'] = $this->AddNewCoach_model->total_Activities("select count(*) total from coachee where IDCoach='" . $_SESSION['coach'] . "'");
            $data['credit'] = $this->coachee_model->get_coach_credit();

            $data['coach_info'] = $this->model_data->get_coach_info($_SESSION['coach']);
            $data['civil_staes'] = $this->model_data->get_civilstates();
            $data['edu'] = $this->model_data->get_education();
            $data['country'] = $this->model_data->get_country();
            $data['hierachi'] = $this->model_data->get_hierachical();
            $data['score'] = $this->model_data->get_total_score($_SESSION['coach'], "idcoach");
        }

        if ($a == "BuyCredits") {
            
        }
        if ($a == "Dashboard") {
            $data['color'] = $this->coachee_model->color_them();
            $data['tips'] = $this->coachee_model->get_tips();
            $data['list_cochee'] = $this->coachee_model->get_list_coachee();
            $data['credit'] = $this->coachee_model->get_coach_credit();
            $data['documents'] = $this->coachee_model->get_coach_documents();
            $data['test'] = $this->coachee_model->get_test();


            $query = "select activities.*,coachee.Name, coachee.IDCoachee as coachee_id,discussions.IDAtividade as 
                        dis_acti_id,discussions.date as date_info from activities 
                        left join discussions on discussions.IDAtividade=activities.IDAtividade 
                        left join coachee on coachee.IDCoachee=activities.IDCoachee
                        where (activities.Deadline<activities.DataEntrega or activities.DataEntrega='0000-00-00') and  activities.IDCoach='" . $_SESSION['coach'] . "' and activities.Deadline<='" . date("Y-m-d") . "'";
            $data['done_activity'] = $this->AddNewCoach_model->total_Activities($query);
        }

        if (isset($_POST['settings'])) {

            mysql_query("UPDATE enable_tips SET Enable='" . $_POST['Enable'] . "', Color='" . $_POST['Color'] . "' WHERE CoachID='" . $_SESSION['coach'] . "'");

            if ($_POST['IDConfig'] == "") {
                $this->coachee_model->add_settings();
                $data['msg'] = $this->model_data->submit_msg();
            } else {
                $this->coachee_model->update_settings($_POST['IDConfig']);
                $data['msg'] = $this->model_data->update_msg();
            }
            header("Location:" . $this->config->base_url() . "index.php/home/index/Settings?msg=" . $data['msg']);
        }

        if ($a == "Tests") {

            $data['list_test'] = $this->test_model->get_list_test();
        }
        if (isset($_POST['test'])) {


            $this->test_model->add_test($b);
            if ($b == "") {
                $data['msg'] = $this->model_data->submit_msg();
                header("Location:" . $this->config->base_url() . "index.php/toolbox?msg=" . $data['msg']);
            } else {

                $data['msg'] = $this->model_data->update_msg();
                header("Location:" . $this->config->base_url() . "index.php/toolbox?msg=" . $data['msg']);
            }
        }

        if ($a == "add_test") {

            $data['test_info'] = $this->test_model->get_info_test($b);
            $data['test_quest'] = $this->test_model->get_info_test_quest($b);
            $data['test_quest'] = $this->test_model->get_info_test_quest($b);
        }
        if ($a == "Tests") {

            $data['list_test'] = $this->test_model->get_list_test();
        }

        if (isset($_POST['wheel'])) {

            $this->test_model->add_wheel($b);
            if ($b == "") {
                $data['msg'] = $this->model_data->submit_msg();
                header("Location:" . $this->config->base_url() . "index.php/toolbox?msg=" . $data['msg']);
            } else {

                $data['msg'] = $this->model_data->update_msg();
                header("Location:" . $this->config->base_url() . "index.php/toolbox?msg=" . $data['msg']);
            }
        }
        if ($a == "add_wheel") {

            $data['test_wheel'] = $this->test_model->get_info_wheel($b);
        }

        if ($a == "Settings") {
            $data['list_languages'] = $this->coachee_model->get_list_languages();
            $data['info'] = $this->coachee_model->get_setting();
            $data['color'] = $this->coachee_model->color_them();
            $data['select'] = $this->coachee_model->select_tip();
        }

        if ($a == "Mydata") {
            $data['get_data_coachee'] = $this->AddNewCoach_model->get_data_coach($_SESSION['coach']);
        }
        $data['code'] = $this->AddNewCoach_model->total_Activities("select code from settings where IDCoach='" . $_SESSION['coach'] . "'");

    $sql_new_coach ="SELECT count(*) as total from settings where IDCoach='".$_SESSION['coach']."'";
       $data['sql_new_coach']=$this->db->query($sql_new_coach)->result();
// $data['sql_new_coach'][0]->total;
$data['total_row']=$data['sql_new_coach'][0]->total;
if($data['sql_new_coach'][0]->total==1){
        $sql_cal ="SELECT is_google_calander from settings where IDCoach='".$_SESSION['coach']."'";
        $data['caldata']=$this->db->query($sql_cal)->result();
        if($data['caldata'][0]->is_google_calander=='0')
        {
             $sql ="SELECT sessions.*,coachee.Name FROM sessions JOIN coachee ON sessions.IDCoachee = coachee.IDCoachee WHERE sessions.IDCoach='".$_SESSION['coach']."'  ORDER BY Date DESC";
            $data['coachData'] =$this->db->query($sql);
        }
    
}
    
        $this->load->view($a, $data);
        $this->load->view('footer', $image);
    }

}
