<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class side_model extends CI_Model {
 function __construct() {
        parent::__construct();
    $this->load->model("language_model");
 }

  
 function coach($lang)
  {
     $rt=$this->language_model->get_languages($lang);

     $data=array(
		array($rt[0],"home/index/Dashboard"),
		array($rt[1],"home/index/Coachees"),
		array($rt[2],"home/index/Tests"),
		array($rt[125],"log_book"),
		array($rt[3],"Documents"),
		array("schedule","home/index/schedule"),
		array($rt[4],"home/index/coachmydata"),
		array($rt[5],"home/index/Settings"),
		array($rt[6],"BuyCredits"),
		array($rt[7],"knowledge"),
		array($rt[124],"report"),
		array($rt[8],"home/index/help"),);
	return $data;
  }


 function coachee($lang)
  {
   $rt=$this->language_model->get_languages($lang);

   
	     $data=array(
		array($rt[0],"home_coachee/"),
	array($rt[125],"logbook_coachee"),
	array($rt[9],"profiler"),
     	array($rt[109],"wheelif"),
		array($rt[10],"MyActivities"),
		array($rt[11],"Testing"),
		array($rt[3],"Documents1/"),
		array($rt[4],"coacheemydata"),
		array($rt[14],"Payments"));
	return $data;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */