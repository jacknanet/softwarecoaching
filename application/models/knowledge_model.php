<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class knowledge_model extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   

     function knowledge()
    {
	    $this->IDCoach  = $_SESSION['coach'];
        $this->Subject = $_POST['Subject']; 
		$this->Description = $_POST['Description']; 
		$this->Date = $_POST['Date']; 
		$this->db->insert('knowledge', $this);
		
	
    }

	   function update_knowledge($id)
    {
	    $this->Subject = $_POST['Subject']; 
		$this->Description = $_POST['Description']; 
		$this->Date = $_POST['Date']; 
		$this->db->where("IDknowledge",$id);
		$this->db->update('knowledge', $this);
		
	
    }
	function list_knowledge()
	{
	   $role1="";
		$this->db->select('*');
		$this->db->from('knowledge');
		$this->db->where("IDCoach",$_SESSION['coach']);
		$query = $this->db->get();
		foreach($query->result() as $role)
		{
		$role1[]=$role;
	   
		}
		return $role1;
	  
	}
	
	function search_knowledge()
	{
	   $role1="";
		$query = $this->db->query("select * from knowledge where Subject like '%".$_POST['term']."%' or Description like '%".$_POST['term']."%' or Date like '%".$_POST['term']."%'");
		foreach($query->result() as $role)
		{
		if($_SESSION['coach']==$role->IDCoach){
		$role1[]=$role;
	   }
		}
		return $role1;
	  
	}
	
	
	function info_knowledge($id)
	{
	   $role1="";
		$this->db->select('*');
		$this->db->from('knowledge');
		$this->db->where('IDknowledge',$id);
		$query = $this->db->get();
		foreach($query->result() as $role)
		{
		$role1[]=$role;
	   
		}
		return $role1;
	  
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */