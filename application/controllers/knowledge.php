<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class knowledge extends CI_Controller {
var $lang="";
function __construct()
{
parent::__construct();
$this->load->model('model_data');
$this->model_data->session_expire_redirect("1");
$this->load->model('language_model');
$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
    
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
     }}


public function index($id="")
{ 
$data['lang']=$this->language_model->get_languages($this->lang);
$this->load->model('knowledge_model');



if(isset($_POST['knowledge']))
{
if($id==""){
$this->knowledge_model->knowledge();
$data['msg']=$this->model_data->submit_msg();
redirect($this->config->base_url()."index.php/knowledge/?msg=".$data['msg']);

}else{
$this->knowledge_model->update_knowledge($id);
$data['msg']=$this->model_data->update_msg();
redirect($this->config->base_url()."index.php/knowledge/?msg=".$data['msg']);
}
}
$data['id']=$id;
if($id!=""){
$data['knowledge_info']=$this->knowledge_model->info_knowledge($id);

}
if(isset($_POST['search']))
{
$data['list_knowledge']=$this->knowledge_model->search_knowledge();
}else{
$data['list_knowledge']=$this->knowledge_model->list_knowledge();

}
$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coach($this->lang);
$image['title'] = 'knowledge';
$image['im']=$current_lang=$this->language_model->get_current_language();

$this->load->view('header',$image);

$this->load->view('knowledgedb',$data);
$this->load->view('footer');

}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */