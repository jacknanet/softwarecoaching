<?php
class language_model extends CI_Model {

function __construct() {
parent::__construct();
}

function get_languages($lang){

return  $this->{"get_".$lang}();
}
function get_English(){
$language=array(
"0"=>"Dashboard","1"=>"Coachees","2"=>"Tests","3"=>"Documents","4"=> "My Data","420"=>"Pending Activities","421"=>"Activity Kind",
"5"=>"Settings","6"=>"Buy Credits","7"=>"Knowledge DB","8"=>"Help","9"=>"Profiler","422"=>"Cancel","423"=>"Add More","424"=>"Update",
"10"=>"My Activities","11"=>"Testing","12"=>"Documents","13"=>"My data","14"=>"Payments","425"=>"Schedule","426"=>"From","427"=>"To","428"=>"Message",
"15"=>"Coachee","16"=>"Credits","17"=>"Documents","18"=>"Tests","19"=>"Active","20"=>"Email","429"=>"Support Phone","430"=>"Download User's Manual",
"21"=>"Phone","22"=>"Name","23"=>"Completion","24"=>"Activity","25"=>"Action","26"=>"Inactive","431"=>"Google calender Code","432"=>"Tip","433"=>"Open",
"27"=>"Completed","28"=>"Add New Coachee","29"=>"Edit Profile","30"=>"Goals","31"=>"Sessions","444"=>"To PayPal Account","445"=>"Done","446"=>"Download","447"=>"Make a Payment",
"32"=>"Activities ","33"=>"Description","34"=>"Date","35"=>"Note","36"=>"Start Date","37"=>"Deadline",
"38"=>"ID","39"=>"Edit","40"=>"Add New Goal","41"=>"Add New Sessions","42"=>" Add New Activities ","43"=>"Activity",
"44"=>"Punctuation","45"=>"Objectives","46"=>"Deadline","47"=>"Difficulty Grade","48"=>"Notes","49"=>"Time",
"50"=>"Evidence","51"=>"Motivators","52"=>"Saboteurs","53"=>"Values","54"=>"Strategies","55"=>"Resources","56"=>"Submit",    
"57"=>"Profile Image","58"=>"Name","59"=>"Email 1","60"=>"Email 2","61"=>" Password","62"=>" Civil State",    
"63"=>"Education","64"=>" Hierachical Level","65"=>" Birth","66"=>"Observations","67"=>"ObjetivosDoCoaching",    
"68"=>"End Coaching Date","69"=>"Score","70"=>"Address","71"=>"Neighborhood","72"=>"City","73"=>"Country",    
"74"=>"Zip Code","75"=>" Phone Residential","76"=>" Phone Commercial","77"=>"Mobile","78"=>" Document",    
"79"=>"Profession","80"=>"Company","81"=>"Start Coaching Date","82"=>" Status","83"=>"Total Sessions",    
"84"=>"Did Sessions","85"=>"Name Rating","86"=>"Add New Test","87"=>"Question","88"=>"Points","89"=>"Type of Field",    
"90"=>"File Name","91"=>"Delete","92"=>"Add a Document","93"=>"File","94"=>"Upload",
"95"=>"Language","96"=>"PayPal Account","97"=>"Logo","98"=>"Total","99"=>"Number of Credit",
"100"=>"Search",  "101"=>"Subject" ,"102"=>"View","103"=>"Result Not Found","104"=>" Add New Knowledge",
"105"=>"Save","106"=>"Knowledge","107"=>"ok","108"=>"Reset","109"=>"WheelLife","110"=>"Mark the satisfaction level to each area in your life",
"111"=>"1  is very bad and 10 is very good","112"=>"Creativity,Hobby and Fun","113"=>"Fullness and Happiness","114"=>"Spirituality",
"115"=>"Health and Disposal","116"=>"Intellectual development","117"=>"Emotional Equilibrium","118"=>"Achievement and Purpose","119"=>"Financial resources",
"120"=>"Social contribution","121"=>"Family","122"=>"Loving development","123"=>"Social life","124"=>"Report","125"=>"LogBook","126"=>"Date","127"=>"Saved LogBook",
"128"=>"Fill Today's LogBook","129"=>"Fill Questions","130"=>"Add New LogBook",
"200"=>"ToolBox","201"=>"Tests&Questionnaires","202"=>"Wheels","203"=>"Add New Wheel","204"=>"Tool","205"=>"Value","206"=>"ToolBox","207"=>"Remove",
"300"=>"Videos and Movies","301"=>"Link","302"=>"Title","303"=>"Purpose","304"=>"Add New Video","305"=>"Video","306"=>"Top and bottom color","307"=>"Registration ID","308"=>"PDF"
    
    


);
return $language;  
}

function get_default(){
$language=array(
"0"=>"Dashboard","1"=>"Coachees","2"=>"Tests","3"=>"Documents","4"=> "My Data","420"=>"Pending Activities","421"=>"Activity Kind",
"5"=>"Settings","6"=>"Buy Credits","7"=>"Knowledge DB","8"=>"Help","9"=>"Profiler","422"=>"Cancel","423"=>"Add More","424"=>"Update",
"10"=>"My Activities","11"=>"Testing","12"=>"Documents","13"=>"My data","14"=>"Payments","425"=>"Schedule","426"=>"From","427"=>"To","428"=>"Message",
"15"=>"Coachee","16"=>"Credits","17"=>"Documents","18"=>"Tests","19"=>"Active","20"=>"Email","429"=>"Support Phone","430"=>"Download User's Manual",
"21"=>"Phone","22"=>"Name","23"=>"Completion","24"=>"Activity","25"=>"Action","26"=>"Inactive","431"=>"Google calender Code","432"=>"Tip","433"=>"Open",
"27"=>"Completed","28"=>"Add New Coachee","29"=>"Edit Profile","30"=>"Goals","31"=>"Sessions","444"=>"To PayPal Account","445"=>"Done","446"=>"Download","447"=>"Make a Payment",
"27"=>"Completed","28"=>"Add New Coachee","29"=>"Edit Profile","30"=>"Goals","31"=>"Sessions",
"32"=>"Activities ","33"=>"Description","34"=>"Date","35"=>"Note","36"=>"Start Date","37"=>"DataEntrega",
"38"=>"ID","39"=>"Edit","40"=>"Add New Goal","41"=>"Add New Sessions","42"=>" Add New Activities ","43"=>"Activity",
"44"=>"punctuation","45"=>"Objectives","46"=>"DataEntrega","47"=>"Difficulty Grade","48"=>"Notes","49"=>"Time",
"50"=>"Evidence","51"=>"Motivators","52"=>"Saboteurs","53"=>"values","54"=>"Strategies","55"=>"Resources","56"=>"Submit",    
"57"=>"Profile Image","58"=>"Name","59"=>"Email1","60"=>"Email 2","61"=>" Password","62"=>" Civil State",    
"63"=>"Education","64"=>" Hierachical Level","65"=>" Birth","66"=>"Observations","67"=>"ObjetivosDoCoaching",    
"68"=>"DataFimCoaching","69"=>"Score","70"=>"Address","71"=>"Neighborhood","72"=>"City","73"=>"Country",    
"74"=>"Zip Code","75"=>" Phone Residential","76"=>" Phone Commercial","77"=>"Mobile","78"=>" Document",    
"79"=>"Profession","80"=>"Company","81"=>"DataInicioCoaching","82"=>" Status","83"=>"SessoesTotal",    
"84"=>"SessoesFeitas","85"=>"Name Rating","86"=>"Add New Test","87"=>"Questions","88"=>"Points","89"=>"Tipo Campo",    
"90"=>"File Name","91"=>"Delete","92"=>"Add a Document","93"=>"File","94"=>"Upload",
"95"=>"Language","96"=>"PayPal Account","97"=>"Logo","98"=>"Total","99"=>"Number of Credit",
"100"=>"Search",  "101"=>"Subject" ,"102"=>"View","103"=>"Result Not Found","104"=>" Add New Knowledge",
"105"=>"Save","106"=>"Knowledge","107"=>"ok","108"=>"Reset","109"=>"WheelLife","110"=>"Mark the satisfaction level to each area in your life.",
"111"=>"1  is very bad and 10 is very good","112"=>"Creativity,Hobby and Fun","113"=>"Fullness and Happiness","114"=>"Spirituality",
"115"=>"Health and Disposal","116"=>"Intellectual development","117"=>"Emotional Equilibrium","118"=>"Achievement and Purpose","119"=>"Financial resources",
"120"=>"Social contribution","121"=>"Family","122"=>"Loving development","123"=>"Social life","124"=>"Report","125"=>"LogBook","126"=>"Date","127"=>"Saved LogBook",
"128"=>"Fill Today's LogBook","129"=>"Fill Questions","130"=>"Add New LogBook",
"200"=>"ToolBox","201"=>"Tests&Questionnaires","202"=>"Wheels","203"=>"Add New Wheel","204"=>"Tool","205"=>"Value","206"=>"ToolBox","207"=>"Remove",
"300"=>"videos and movies","301"=>"Link","302"=>"Title","303"=>"Purpose","304"=>"Add New Video","305"=>"Video","306"=>"Top and bottom color","307"=>"Registration ID","308"=>"PDF"
    


);
return $language;  
}
function get_Spanish(){
$language=array(
"0"=>"salpicadero","1"=>"coachees","2"=>"pruebas","3"=>"Documentos","4"=>"Misdatos","420"=>"Actividades pendientes","421"=>"Actividad Kind",
"5"=>"Configuración","6"=>"Comprarcrédito","7"=>"ElconocimientoDB","8"=>"ayuda","9"=>"Perfil","422"=>"cancelar","423"=>"Añadir más","424"=>"actualización",
"10"=>"Misactividades","11"=>"Testing","12"=>"Documentos","13"=>"Misdatos","14"=>"Pagos","425"=>"horario","426"=>"desde","427"=>"a","428"=>"mensaje",
"15"=>"Coachee","16"=>"Créditos","17"=>"Documentos","18"=>"Pruebas","19"=>"Activo","20"=>"Correo electrónico","429"=>"Soporte Telefónico","430"=>"Descargar Manual del Usuario",
"21"=>"Teléfono","22"=>"Nombre","23"=>"Finalización","24"=>"Actividad","25"=>"Acción","26"=>"Inactivo","431"=>"Código calendario Google",
"27"=>"Completado","28"=>"AgregarnuevoCoachee","29"=>"Editarperfil","30"=>"Objetivos","31"=>"Sesiones","432"=>"punta","433"=>"abierto","444"=>"Para cuenta PayPal","445"=>"Hecho",
"32"=>"Actividades","33"=>"Descripción","34"=>"fecha","35"=>"Nota","36"=>"FechadeInicio","37"=>"DataEntrega","446"=>"descargar","447"=>"Hacer un Pago",
"38"=>"ID","39"=>"Editar","40"=>"AgregarNuevoobjetivo","41"=>"AgregarnuevoSessions","42"=>"AgregarNuevasActividades","43"=>"Actividad",
"44"=>"puntuacion","45"=>"Objetivos","46"=>"DataEntrega","47"=>"Gradodedificultad","48"=>"Notas","49"=>"tiempo",
"50"=>"Evidence","51"=>"Motivators","52"=>"Saboteurs","53"=>"values","54"=>"Strategies","55"=>"Resources","56"=>"presentar",
"57"=>"Imagendelperfil","58"=>"Nombre","59"=>"Correoelectrónico1","60"=>"Correoelectrónico2","61"=>"Contraseña","62"=>"EstadoCivil",
"63"=>"Laeducación","64"=>"Niveljerárquico","65"=>"nacimiento","66"=>"Observaciones","67"=>"ObjetivosDoCoaching",
"68"=>"DataFimCoaching","69"=>"¡Puntuación","70"=>"dirección","71"=>"Neighborhood","72"=>"ciudad","73"=>"país",
"74"=>"Códigopostal","75"=>"Teléfonoresidencial","76"=>"TeléfonoComercial","77"=>"Móvil","78"=>"Documento",
"79"=>"Profesión","80"=>"Compañía","81"=>"DataInicioCoaching","82"=>"Estado","83"=>"SessoesTotal",
"84"=>"SessoesFeitas","85"=>"NombreOpinión","86"=>"Agregarnuevaprueba","87"=>"pregunta","88"=>"Puntos","89"=>"TipoCampo",
"90"=>"Nombredearchivo","91"=>"Eliminar","92"=>"Añadirundocumento","93"=>"Archivo","94"=>"Subir",
"95"=>"Idioma","96"=>"CuentadePayPal","97"=>"Logo","98"=>"Total","99"=>"NúmerodeCrédito",
"100"=>"Buscar","101"=>"Asunto","102"=>"Ver","103"=>"Elresultadonoencontrado","104"=>"Agregarnuevoconocimiento",
"105"=>"Guardar","106"=>"conocimiento","107"=>"Okay","108"=>"Restablecer","109" => "WheelLife", "110" => "Marcar el nivel de satisfacción a cada área de tu vida.",
"111"=>"1 es muy malo y 10 es muy bueno", "112" => "La creatividad, Hobby y diversión", "113" => "La plenitud y la felicidad", "114" => "Espiritualidad",
"115"=>"La salud y Reciclaje", "116" => "El desarrollo intelectual", "117" => "Equilibrio Emocional", "118" => "Aprovechamiento y Purpose", "119" => "Los recursos financieros ",
"120"=>"contribución social", "121" => "La familia", "122" => "Amar desarrollo", "123"=>"La vida social", "124" => "Informe",
"125"=>"Cuaderno de bitácora","126"=>"Fecha","127"=>"Guardados logBook",
"128"=>"Llenar el diario de Hoy", "129" => "Fill Preguntas","130"=>"Agregar nuevo LogBook",
"200"=>"ToolBox", "201"=>"Pruebas y Cuestionarios","202"=>"ruedas","203"=>"Añadir Nueva Rueda","204"=>"herramienta","205"=>"valor","206"=>"Caja de herramientas","207"=>"quitar",
"300"=>"videos y películas","301"=>"enlace","302"=>"título","303"=>"propósito","304"=>"Añadir un Nuevo Vídeo","305"=>"vídeo","306"=>"Top y el color de fondo","307"=>"Matricula","308"=>"PDF"
);
return $language;  
}


function get_current_language(){
	$role1;
	    $query = $this->db->query("select languages.*,settings.Logo from settings 
                inner join languages on languages.id=settings.Language
where  settings.IDCoach='".$_SESSION['coach']."'");
	 
	 //echo $this->db->last_query();
	 foreach($query->result() as $role){

	 $role1[]=$role;
	 }
       return $role1;
	   
	
}

function get_coachee_current_language(){
    
   
     $role1;
	    $query = $this->db->query("select languages.*,settings.Logo, coach.IDCoach as coach123 from settings 
                inner join languages on languages.id=settings.Language
               inner join coach on coach.IDCoach=settings.IDCoach
			   inner join coachee on coachee.IDCoach=coach.IDCoach
			   where  coachee.IDCoachee='".$_SESSION['coach']."'");
	 
	 //echo $this->db->last_query();
	 foreach($query->result() as $role){

	 $role1[]=$role;
	 }
       return $role1;
	   
	
}

function get_Portuguese(){
    
 $lang= array("0"=>"Painel","1"=>"Coachees","2"=>"Testes","3"=>"Documentos","4"=>"Meus Dados","420"=>"Atividades Pendentes","421"=>"Tipo Atividade",
"5"=>"Configurações","6"=>"Comprar Créditos","7"=>"Conhecimento","8"=>"Ajuda","9"=>"Profiler","422"=>"cancelar","423"=>"Adicionar mais","424"=>"atualizar",
"10"=>"Minhas Atividades","11"=>"Teste","12"=>"Documentos","13"=>"Meus dados","14"=>"Pagamentos","425"=>"Agenda","426"=>"a partir de","427"=>"para","428"=>"mensagem",
"15" =>"Coachee","16"=>"Créditos","17"=>"Documentos","18"=>"Testes","19" => "Ativo","20" => "E-mail","429"=>"Suporte por telefone","430"=>"Download do Manual do Usuário",
"21"=>"Telefone","22"=>"Nome","23"=>"Conclusão","24"=>"Atividade","25"=>"Ação","26"=>"Inativo","431"=>"Google Code calendário","432"=>"ponta","433"=>"aberto",
"27"=>"Concluído","28"=>"Adicionar Novo Coachee","29"=>"Editar Perfil","30"=>"Objetivos","31"=>"Sessões","444"=>"Para Conta PayPal","445"=>"Feito","446"=>"baixar","447"=>"Faça um Pagamento",
"32"=>"Atividades","33"=>"Descrição","34"=>"Data","35"=>"Nota","36"=>"Data de Início","37"=>"Data Entrega",
"38"=>"ID","39"=>"Editar","40"=>"Adicionar Novo Objetivo","41"=>"Adicionar Nova Seção","42"=>"Adicionar nova Atividade","43"=>"Atividade",
"44"=>"Pontuação","45"=>"Objetivos","46"=>"Data Entrega","47"=>"Dificuldade","48"=>"Notas","49"=>"tempo",
"50"=>"Evidência","51"=>"Motivadores","52"=>"Sabotadores","53"=>"values","54"=>"Estratégias","55"=>"Recursos","56"=>"submeter",
"57"=>"Imagem do Perfil","58"=>"Nome","59"=>"E-mail 1","60"=>"E-mail 2","61"=>"Senha","62"=>"Estado Civil",
"63"=>"Educação","64"=>"Nível hierárquico","65"=>"Nascimento","66"=>"Observações","67"=>"Objetivos Do Coaching",
"68"=>"Data Fim Coaching","69"=>"Pontos","70"=>"Endereço","71"=>"Bairro","72"=>"Cidade","73"=>"País",
"74"=>"CEP","75"=>"Telefone Residencial","76"=>"Telefone Comercial","77"=>"Celular","78"=>"Documento",
"79"=>"Profissão","80"=>"Empresa","81"=>"Data Inicio Coaching","82"=>"Status","83"=>"Sessões Total",
"84"=>"Sessões Feitas","85"=>"Nome Avaliação","86"=>"Adicionar Novo Teste","87"=>"Pergunta","88"=>"Pontos","89"=>"Tipo Campo",
"90"=>"Nome do arquivo","91"=>"Excluir","92"=>"Adicionar Documento","93"=>"Arquivo","94"=>"Upload",
"95"=>"Idioma","96"=>"Conta PayPal","97"=>"Logotipo","98"=>"Total","99"=>"Número de créditos",
"100"=>"Pesquisar","101"=>"Assunto","102"=>"Exibir","103"=>"Resultado não encontrado","104"=>"Adicionar novo conhecimento",
"105"=>"Salvar","106"=>"Base Conhecimento","107"=>"Está bem","108"=>"Reset",
"109" => "Roda da Vida", "110" => "Marcar o nível de satisfação a cada área de sua vida.",
"111" => "1 é péssimo e 10 é muito bom", "112" => "Criatividade, Hobby e Diversão", "113" => "Plenitude e Felicidade", "114" => "Espiritualidade",
"115" => "Saúde e Eliminação", "116" => "O desenvolvimento intelectual",
 "117" => "Equilíbrio Emocional", "118" => "Realização e Propósito", "119" => "Recursos financeiros ",
"120" => "Contribuição Social", "121" => "Família", "122" => "Relacionamento Amoroso", "123" => "A vida social", "124" => "Relatório", "125"=>"Diário de Bordo",
"126"=>"Data","127"=>"Diários salvos",
"128" => "Preencha o Diário de Bordo hoje", "129" => "Responda às Perguntas","130"=>"Adicionar novo Diário de Bordo",
"200"=>"Caixa de Ferramentas","201"=>"Testes e Questionários","202"=>"Rodas da Vida","203"=>"Adicionar Nova Roda","204"=>"Ferramenta","205"=>"Valor","206"=>"Caixa de Ferramentas","207"=>"remover",
"300"=>"Vídeos e Filmes","301"=>"Link","302"=>"Título","303"=>"Propósito","304"=>"Adicionar um Novo Vídeo","305"=>"vídeo","306"=>"Top e cor de fundo","307"=>"Matrícula","308"=>"PDF"); 
return $lang;
 
}

function payment_add(){

$value->IDCoachee=$_SESSION['coach'];
$value->Date=date("Y-m-d");
$value->Hour=date("H:i:s");
$value->Value=$_POST['amount_1'];
$value->From=$_SESSION['coach'];
$this->db->insert("payment",$value);

}
}


