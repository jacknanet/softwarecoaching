<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="description" content="User login page" />
    <meta name="author" content="Coachcoachee">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Coaching System</title>
    <!-- Font CSS (Via CDN) 
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
	-->
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->

    <!-- BEGIN: PAGE SCRIPTS -->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

	<!--jqueryvalidation plugin-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/validate/validate.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/additional-methods.js"></script>
	
	<!-- CÓDIGO ORIGINAL -->
	<!-- 
	<script>
		$(document).ready(function(){

				
			$('#frmlogsec').validate();
			$('#frmregsec').validate({
				rules: {
					Name:{
						alphanumeric: true
					},
					Password: {
						alphanumeric: true,
						minlength: 6,
						maxlength: 8
					},
					confpassword:{
						equalTo: "#Password"
					},
					Email1:{
						customemail: true
					}
					
				},
				messages: {
					confpassword: 'Password & confirm password should be same'
				}
				
			})
		});
	</script> -->
	<!-- -->
	
	
	<script>
		$(document).ready(function(){

				
			$('#frmlogsec').validate();
			$('#frmregsec').validate({
				rules: {
					
					Password: {
						alphanumeric: true,
						minlength: 6,
						maxlength: 8
					},
					confpassword:{
						equalTo: "#Password"
					},
					Email1:{
						customemail: true
					}
					
				},
				messages: {
					confpassword: 'Password & confirm password should be same'
				}
				
			})
		});
	</script>
</head>

<body class="external-page sb-l-c sb-r-c">

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- begin canvas animation bg -->
            <div id="canvas-wrapper">
                <canvas id="demo-canvas" width="1366" height="657"></canvas>
            </div>

            <!-- Begin: Content -->
            <section id="content">
			<?php if($id=="login" || $id==""){ ?>

                <div class="admin-form theme-info" id="login1">
                    <div class="panel panel-info mt10 br-n">
                        <div class="panel-heading heading-border bg-white">
                          
                            <div class="section row mn">
                                <div class="col-sm-12">
                                    <a href="#" class="button btn-social facebook span-left mr5 btn-block">
                                        <span><i class="fa fa-group"></i>
                                        </span>Login</a>
                                </div>
                                
                            </div>
                        </div>

                        <!-- end .form-header section -->
						<?php if(!empty($msg)) {?>
							<div class="alert alert-danger dark alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-check pr10"></i>
								<strong>&nbsp; </strong> 
								<a href="#" class="alert-link"> <?php echo $msg; ?> </a>
							</div>
							<?php }?>
                        <form id="frmlogsec" method="post" action="">
                            <div class="panel-body bg-light p30">
                                <div class="row">
                                    <div class="col-sm-7 pr30">

                                        <div class="section">
                                            <label for="username" class="field-label text-muted fs18 mb10">Email </label>
                                            <label for="username" class="field prepend-icon">
                                                <input type="email" name="Email1" id="username" class="gui-input" placeholder="Email"  required >
                                                <label for="username" class="field-icon"><i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                        <div class="section">
                                            <label for="username" class="field-label text-muted fs18 mb10">Senha</label>
                                            <label for="password" class="field prepend-icon">
                                                <input type="password" name="Password" id="password" class="gui-input" placeholder="Senha" required>
                                                <label for="password" class="field-icon"><i class="fa fa-lock"></i>
                                                </label>
                                            </label>
                                        </div>
			
                                        <!-- end section -->

                                    </div>
                                    <div class="col-sm-5 br-l br-grey pl30">
                                        <h3 class="mb25"> Importante:</h3>
                                        <p class="mb15">
                                            <span class="fa fa-check text-success pr5"></span> Se esqueceu sua senha, </p>
                                        <p class="mb15">
                                            <span class="fa fa-check text-success pr5"></span> Clique no link Esqueci Senha, abaixo </p>
                                        <p class="mb15">
                                            <span class="fa fa-check text-success pr5"></span> Se é um Novo Administrador, </p>
                                        <p class="mb15">
                                            <span class="fa fa-check text-success pr5"></span> Clique em Novo Registro  </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix p10 ph15">
							<div class="row abc">
							<div class="col-xs-4">
							<ul class="nav nav-pills">
							   <li>
                                <button type="submit" name="login" class="button btn-primary pull-left">Login</button>
								</div>
								</li>
								</ul>
								<div class="col-xs-8">
                               <!-- <label class="switch block switch-primary pull-right input-align mt10">   -->
							   <ul class="nav nav-pills pull-right">
							   <li>
    							<a href='<?php echo $this->config->base_url(); ?>index.php/login/index/forget' style="padding-top:0px;"> 
									Esqueci Senha </a> </li>
									<li style="margin-left:0px;">
									<a href='<?php echo $this->config->base_url(); ?>index.php/login/index/register' style="padding-top:0px;">
									Novo Registro </a></li>
									</ul>
                               <!-- </label>  -->
								</div>
								</div>
                            </div> 	
							
							
                            <!-- end .form-footer section -->
                        </form>
                    </div>
                </div>
			<?php } if($id=="forget"){?>
				 <div class="admin-form theme-info mw500" style="margin-top: 10%;" id="login1">
                    <div class="row mb15 table-layout">

                        <div class="col-xs-8 center-block">
                            <a href="#" title="Return to Dashboard" style='text-decoration:none;'>
                                <h1 style="color:#FFF">Esqueci Senha </h1>
                            </a>
                        </div>

                        <div class="col-xs-4 text-right va-b pr5">
                            <div class="login-links">
                                <a href="<?php echo $this->config->base_url(); ?>index.php/login" class="" title="Login Page">Voltar ao Login</a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info mv10 heading-border br-n">

                        <!-- end .form-header section -->
                        <form action="" method="POST">
                            <div class="panel-body bg-white p15 pt25">
								<div class="col-sm-12">
                                    <a href="#" class="button btn-social twitter span-left mr5 btn-block">
                                        <span><i class="fa fa-group"></i>
                                        </span>Recuperar Senha </a>
                                </div>
						
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer p25 pv15">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section mn">
                                            <label for="email" class="field-label text-muted fs18 mb10 hidden">Recuperar Senha</label>
											<label style="color:#3BAFDA;"> Informe seu Email para recuperar a senha  </label>
                                            <div class="smart-widget sm-right smr-80">
												
                                                <label for="email" class="field prepend-icon">
                                                    <input type="text" name="Email1" required  class="gui-input" placeholder="Email">
                                                    <label for="email" class="field-icon"><i class="fa fa-envelope-o"></i>
                                                    </label>
                                                </label>
												
                                                <button name="sendme"  class="button btn-info">Enviar </button>
                                            </div>
                                            <!-- end .smart-widget section -->
                                        </div>
                                        <!-- end section -->
										
                                    </div>
									
                                </div>
								
                            </div>
							<?php
							if(isset($_POST['sendme'])){
							if(strlen( $infor[0]->Password)>1){ ?>
							<div class="alert alert-info dark alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-check pr10"></i>
								<strong>&nbsp; </strong> 
								<a href="#" class="alert-link"> Sua Senha e: <?php echo $infor[0]->Password; ?> </a>
							</div>
							<?php }else{ ?>
							<div class="alert alert-danger dark alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-check pr10"></i>
								<strong>&nbsp; </strong> 
								<a href="#" class="alert-link"> Email não existe </a>
							</div>
							<?php }} ?>
							
                            <!-- end .form-footer section -->
                        </form>

                    </div>

                </div>
			<?php } if($id=="register"){ ?>
				<div class="admin-form theme-info mw700" style="margin-top: 3%;" id="login1">

                    <div class="row mb15 table-layout">

                        <div class="col-xs-6 va-m pln">
                            <a href="#" style='text-decoration:none;'>
                              <h1 style="color:#FFF">Formulário de Cadastro </h1>
                            </a>
                        </div>

                        <div class="col-xs-6 text-right va-b pr5">
                            <div class="login-links">
                                <a href="<?php echo $this->config->base_url(); ?>index.php/login" class="" title="Login Page">Voltar ao Login</a>
                               
                            </div>

                        </div>

                    </div>

                    <div class="panel panel-info mt10 br-n">

                        <div class="panel-heading heading-border bg-white">
                            <div class="section row mn">
                               
                                <div class="col-sm-12">
                                    <a href="#" class="button btn-social twitter span-left btn-block">
                                        <span><i class="fa fa-group"></i>
                                        </span> Cadastro de novo Coach </a>
                                </div>
                            </div>
                        </div>

                        <form id="frmregsec" action="" method="post">
							<?php if(!empty($msg)) { ?>
							<div class="alert alert-info dark alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-check pr10"></i>
								<strong>&nbsp; <?php echo $msg ?> </strong> 
								
							</div>
							<?php }?>
							
                            <div class="panel-body p25 bg-light">
                                <div class="section-divider mt10 mb40">
                                    <span>Informe seus dados abaixo</span>
                                </div>
                                <!-- .section-divider -->

                               <!-- <div class="section row">
                                    <div class="col-md-12">
                                        <label for="firstname" class="field prepend-icon">
                                            <input type="text" name="Name"  class="gui-input" placeholder="User name..." required="required"/>
                                            <label for="firstname" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
								</div> -->
                                    <!-- end section -->
									
								<div class="section">
                                    <label for="email" class="field prepend-icon">
                                        <input type="email" id="Email1" name="Email1" required="required" class="gui-input" placeholder="Email">
                                        <label for="email" class="field-icon"><i class="fa fa-envelope"></i>
                                        </label>
                                    </label>
                                </div>
                                <!-- end section -->

                                <div class="section">
                                    <label  class="field prepend-icon">
                                        <input type="text" name="Mobile" required="required" class="gui-input" 
										placeholder="Telefone" onKeyUp="this.value=this.value.replace(/[^0-9\.]/g,'');">
                                        <label class="field-icon"><i class="fa fa-phone"></i>
                                        </label>
                                    </label>
                                </div>
                                <!-- end section -->
								<div class="section row">
                                    <div class="col-md-12">
                                        <label for="password" class="field prepend-icon">
											<input type="password" id="Password" name="Password" required="required" class="gui-input" placeholder="Senha"/>
											<label for="password" class="field-icon"><i class="fa fa-unlock-alt"></i>
											</label>
										</label>
                                    </div>
                                    <!-- end section -->
                                </div>
                                <!-- end .section row section -->
								<div class="section row">
                                    <div class="col-md-12">
                                        <label for="password" class="field prepend-icon">
											<input type="password" id="confpassword" name="confpassword" required="required" class="gui-input" placeholder="Confirmação de senha"/>
											<label for="confpassword" class="field-icon"><i class="fa fa-unlock-alt"></i>
											</label>
										</label>
                                    </div>
                                    <!-- end section -->
                                </div>
								 <!-- end .section row section -->
                                

                                <div class="section-divider mv40">
                                    <span>Termos de Uso</span>
                                </div>
                                <!-- .section-divider -->

                                <div class="section mb15">
                                   
                                    <label class="option block mt15">
                                        <input type="checkbox" name="terms" checked required>
                                        <span class="checkbox"></span>Eu aceito os
                                        <a href="#" class="smart-link" data-toggle="modal" data-target="#termcond"> termos de uso. </a>
                                    </label>
                                </div>
                                <!-- end section -->

                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix">
                                <button type="submit" name="register" class="button btn-info pull-right"> Registrar </button>
                            </div>
							
                            <!-- end .form-footer section -->
                        </form>
                    </div>
					
                </div>


			<?php }?>
            </section>
			
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->




<!-- Modal -->
<div id="termcond" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Termos de Uso</h4>
      </div>
      <div class="modal-body">
        <p>Todos os direitos autorais deste software são devidos à Solides Tecnologia S/A</p>
	<p>Para adicionar um novo Coachee no software, o usuário deverá contratar mais créditos em sua conta. Será consumido 1 (um) crédito para cada Coachee cadastrado.</p>
	<p>O Coach é responsável por cada material ou ferramenta, documento ou texto que ele criou ou adicionou dentro do software e responderá judicialmnte isoladamente pela utilização das mesmas</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>


    </div>
    <!-- End: Main -->



    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- Page Plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/EasePack.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/rAF.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/TweenLite.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/login.js"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/utility/utility.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/demo.js"></script>


	
	
    <!-- Page Javascript -->
   <script type="text/javascript">
        jQuery(document).ready(function() {

            
            // Init CanvasBG and pass target starting location
            CanvasBG.init({
                Loc: {
                    x: window.innerWidth / 2,
                    y: window.innerHeight / 3.3
                },
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
