<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class BuyCredits extends CI_Controller
{
    var $lang = "";
    
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('model_data');
        $this->load->model('Coach_model');
        $this->model_data->session_expire_redirect("1");
        $this->load->model('language_model');
        $current_lang = $this->language_model->get_current_language();
        
        if ($current_lang[0]->name != "") {
            $this->lang = $current_lang[0]->name;
        } else {
            $this->lang = "default";
            
        }
        
    }
    public function index($a = "", $b = "")
    {
        $image['title'] = $a;
        $data['lang']   = $this->language_model->get_languages($this->lang);
        $image['im']    = $current_lang = $this->language_model->get_current_language();        

        $this->load->view('header', $image);
        $this->load->model('sidebar/side_model');
        $data['side'] = $this->side_model->coach($this->lang);
        if (isset($_POST['search'])) {
            $a              = $this->model_data->save_tmp_credit();
            $data['view']   = $a;
            $data['amount'] = $_POST['amount_2'];
        }

        $data['is_cliente'] = $this->Coach_model->is_cliente_solides($_SESSION['coach']);	
                
        $this->load->view("BuyCredits", $data);
        $this->load->view('footer', $image);
        
    }

    public function checkout_pagseguro()
    {
        
        $idclifor = $this->Coach_model->is_cliente_solides($_SESSION['coach']);
        $cod_transacao = get_cod_transacao_pagseguro($this->input->post("quantidade"), $idclifor, "dev");

        redirect("https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=".$cod_transacao['message']);
        
    }
    
    
    
}