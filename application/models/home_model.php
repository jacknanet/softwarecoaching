<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model {

	var $title   = '';
	var $content =array();
	var $check_login =array();
	var $date    = '';

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function login()
	{
		$row="";

		$result1=$this->db->query("select IDCoach as id ,role from coach where Email1='".addslashes(trim($_POST['Email1']))."' and Password='".addslashes(trim($_POST['Password']))."'");
		$result2=$this->db->query("select IDCoachee as id,IDCoach as coach_id,role  from coachee where Email1='".addslashes(trim($_POST['Email1']))."' and Password='".addslashes(trim($_POST['Password']))."' and ((DataInicioCoaching < now() or DataInicioCoaching is null or DataInicioCoaching = '') and (DataFimCoaching > now() or DataFimCoaching is null or DataFimCoaching = ''))");
		$result3=$this->db->query("select IDCoachee as id,IDCoach as coach_id,role,DataInicioCoaching,DataFimCoaching  from coachee where Email1='".addslashes(trim($_POST['Email1']))."' and Password='".addslashes(trim($_POST['Password']))."'");
 
		foreach($result1->result() as $d)
		{
			$this->content[]=$d;
		}
		foreach($result2->result() as $d)
		{
			$this->content[]=$d;
		}
		if(count($result2->result()) == 0 && count($result3->result()) > 0){
			$coachee = $result3->result()[0];
			$this->content[]="Acesso negado: fora do período permitido para o acesso que é entre ".$coachee->DataInicioCoaching." e $coachee->DataFimCoaching";
		}
		return $this->content;
	}

	function check_login_first_time($field,$id,$date)
	{
		$row;
		$result1=$this->db->query("select idlog  from log_gamification where ".$field."='".$id."' and date='".$date."'");
		foreach($result1->result() as $d)
		{
			$this->check_login[]=$d;
		}

		return $this->check_login;
	}
	function check_sign_in_count($id){
		$result1=$this->db->query("select SignInCount from coachee where IDCoachee='".$id."'");
		return $result1->result();
	}

	function  get_role($RoleID="")
	{$role1="";
		$this->db->select('RoleID,Role');
		$this->db->from('role');
		//$this->db->where('RoleID',$RoleID);
		$query = $this->db->get();

		//echo $this->db->last_query();
		foreach($query->result() as $role){

			$role1.="<option value='".$role->RoleID."'>".$role->Role."</option>";
		}
		return $role1;

	}

	function  get_activity_info($id)
	{$role1;
		$query = $this->db->query("select * from  activities where IDAtividade='".$id."'");


		foreach($query->result() as $role){
			$role1[]=$role;
		}
		return $role1;

	}



	function save_discussion()
	{  
		$query=$this->db->query("select name from coachee where IDCoachee='". $_POST['IDCoachee']."'");
		foreach($query->result() as $role){
			$role1[]=$role;
		}

		$subject = "New Disccussion";
		$txt.= "<table border='0'>";
		$txt.= "<tr><td>Coachee Name:</td><td>".$role1[0]->name."</td></tr>";

		$txt.= "<tr><td>Massage:</td><td>".addslashes(trim($_POST['msg']))."</td></tr>";

		$txt.= "</table>";


		$headers = "From:";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";


		mail($_POST['email'],$subject,$txt,$headers);

		date_default_timezone_set("Asia/Kolkata"); 
		
		$data1 = new stdClass();
		$data1->IDCoach  = $_POST['IDCoach'];
		$data1->IDCoachee  = $_POST['IDCoachee'];
		$data1->IDAtividade = $_POST['IDAtividade']; 
		$data1->Date = date("Y-m-d"); 
		$data1->Time = date("H:i:s"); 
		$data1->Discussion = addslashes(trim($_POST['msg'])); 
		$data1->Response = $_POST['Response']; 
		$this->db->insert('discussions', $data1);
	
	}
	function get_discussion($IDAtividade)
	{
	 	
		$role1;
        
		$query = $this->db->query("select
			coachee.Name as coachee_Name,
		coach.Name as coach_Name,
		coach.Photo as coach_Photo,
		coachee.Photo as coachee_Photo,
		discussions.Date,
		discussions.Time,
		discussions.Discussion,
		discussions.Response 
		from  discussions 
		left join  coach on coach.IDCoach=discussions.IDCoach
		left join coachee on coachee.IDCoachee=discussions.IDCoachee
		where discussions.IDAtividade='".$IDAtividade."'order by discussions.IDDiscussao desc");


		foreach($query->result() as $role){
			$role1[]=$role;
		}
		return $role1;

	}

	
	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */