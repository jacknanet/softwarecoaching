<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Coach_model extends CI_Model {


	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->solides_master_db = $this->load->database('solides_master_devel', TRUE); 

	}

	public function is_cliente_solides($coach_id)
	{
		$idclifor = $this->db->get_where("coach", array("IDCoach" => $coach_id))->row_array()['idclifor'];
		if($idclifor > 0){
			return $idclifor;
		}

		return FALSE;

	}


}