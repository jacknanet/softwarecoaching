<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<?php
		$menu = $lang[447];
		include ('topbar.php');
	?>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">

<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[447]?>
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							
							<form class="form-horizontal" action="" method="post">
							<div class="col-md-6">
								<div class="admin-form">
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard">Total $:</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="amount_1" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');" required>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[444]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control total" value="<?=$test_info[0]->ContaPaypal?>" readonly>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-8">
									<button class="btn active btn-success" type="submit" name="pay">
									<i class="fa fa-credit-card"></i>  Paypal </button>
									
									</div>
									</div>
								
								
								</div>	
							</div>
							</form>
						
                        </div>
                    </div>
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
</script>
<script>
$(document).ready(function(){

$(".credit").keyup(function(){
$(".total").val($(this).val()*100);
});
});
</script>
